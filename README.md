## Explanation of data files on FigShare:
density_file_reference.txt: This file describes which projects and files were used for the MS2 peak extraction (Figure 9 + Supplementary Figures 8-9)

density+fragmentation.zip: This contains the txt files needed to create the ms2 density plots (figures 9 and 8s+9s) and the fragmentation figures 

files.bson.gz: This is a MongoDB file which contain the metadata from all files used during the analysis.

projects.bson.gz: This is a MongoDB file which contain the metadata from all projects on PRIDE (as of Feburary 2023). Not all metadata were used in the manuscript

models.7z: This is a zipped folder of all trained models we discussed in the manuscript and the parameters MongoDB used by MS2AI. If these are extracted into the MS2AI metadata folder they will be runable with the -tm or -t functions (read the MS2AI documentation)

dbs.7z: This contains 12 MongoDB datasets used to train each of the models in models.7z. The datasets are described in Table 1.

## Figures
Creatable figures using just the provided material: 3-10

Creatable supplementary figures just the provided material: 1-4 + 6-15

Figures 1 and supplementary 5 are illustration made in external programs and figure 2 requires the full 150M dataset (which is not appended in the dbs.7z). To re-create figure 2 you need to run MS2AI with the -p options as mention in the manuscript

## Code explanation
As expained in the manuscript, the main codebase used in this analysis is [MS2AI](https://gitlab.com/roettgerlab/ms2ai)
Which downloaded the raw files, handled files and metadata, and created the MongoDB datasets.
Some figures (Figure 6+8) are also created in MS2AI source code and explained in the manuscript methods.

### Expanation of files:

analysis_plots: This file creates the plots for gradients, m/z filters, and score distribution of the MS2AI database (figures 2, 5, 7)

find_projects: This file contains the filters used to subdivide the larger 150M dataset into the smaller datasets in table 1

cloud_training: Use to train, validate, test, refine and combine output files. This creates the models and files used for figures 3, 4, 6, 8 as well as supplementary figures 1-4 + 6-7. 
This file also contains all seeds and automatic splitting used for model training and testing.

model_plots: uses the output files from cloud_computing to create the figures.

ms2_density: computes the ms2 density plots (figures 9 and supp. 8 and 9)

fragmentation: creates the ms2 fragmentation plots (figures 10 and supp. 10-15)

### How to run:
* Get MS2AI and setup the base folders and MongoDB
* Download and extract files from FigShare
* import the files from dbs.7z into MongoDB using "mongorestore --gzip --archive='FILE_NAME'" as well as files.bson.gz, projects.bson.gz, and the model_parameters.bson.gz from the model.7z
* Setup a directory for the files in density+fragmentation and copy files into the directory.
* Copy model files from model.7z into the MS2AI metadata folder.
* analysis_plots and model_plots works base with MongoDB. ms2_density and framgentation plots needs to be run with 'file.py /link/to/path'
* To create Figure 2 you need to run MS2AI with the -p option. This will take a long time, and create a database of 100+ GB.
