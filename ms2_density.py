import json
import os
import sys
from collections import Counter, defaultdict
from functools import partial
from glob import glob
from itertools import chain
from multiprocessing import Pool

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from msgpack import unpackb, packb


def __create_file(path):
    if not os.path.exists(path):
        os.makedirs(path)


def __from_byte(obj):
    return unpackb(obj, raw=False)


def __to_byte(obj):
    return packb(obj)


def read(name):
    with open(name, 'rb') as file:
        return __from_byte(file.read())


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(__to_byte(obj))


def get_aa_weights(data):
    a = Counter(data)
    b = a.most_common()

    aa_alphabet = 'ARNDCEQGHILKMFPSRWYV'
    names = ['Alanine', 'Arginine', 'Asparagine', 'Aspartate', 'Cysteine', 'Glutamate', 'Glutamine', 'Glycine', 'Histidine', 'Isoleucine', 'Leucine', 'Lysine', 'Methionine', 'Phenylalanine', 'Proline', 'Serine', 'Threonine', 'Tryptophan', 'Tyrosine', 'Valine']
    # http://www2.riken.jp/BiomolChar/Aminoacidmolecularmasses.htm
    weights = [71.1, 156.2, 114.1, 115.1, 103.1, 129.1, 128.1, 57.1, 137.1, 113.1, 113.1, 128.1, 131.2, 147.1, 97.1, 87.1, 101.1, 186.2, 163.1, 99.1]
    # http://db.systemsbiology.net/proteomicsToolkit/FragIonServlet.html
    a_weights = [round(f - 27, 1) for f in weights]
    b_weights = [round(f + 1, 1) for f in weights]
    y_weights = [round(f + 19, 1) for f in weights]
    c_weights = [round(f + 18, 1) for f in weights]
    z_weights = [round(f + 2, 1) for f in weights]

    breakoff_point = 1
    c_w = [f[0] for f in b if f[1] / len(data) * 100 > breakoff_point]
    c_w = list(chain(*[[round(f - 0.09, 1), f, round(f + 0.11,1)] for f in c_w]))
    a = [[names[i] + f"({ff})", g] for f, ff in zip([a_weights, b_weights, y_weights, c_weights, z_weights], ['a', 'b', 'y', 'c', 'z']) for i, g in enumerate(f) if g in c_w]
    c = defaultdict(list)
    [c[f].append(g[0]) for f in list(np.unique([f[1] for f in a])) for g in a if g[1] == f]
    c = [[f, ' | '.join(ff)] for f, ff in zip(list(c.keys()), list(c.values()))]
    return [[[round(f[0] - 0.09,1), f[0], round(f[0] + 0.11,1)], f[1]] for f in c]


def plot_topn(path, i, bins):
    """
    Plots 4 histograms of peaks in ms2 spectra.
    The four plots differ in the amount of peaks taken from each ms2 spectra:
        Top 50 most intense peaks
        Top 100 most intense peaks
        Top 200 most intense peaks
        All peaks
    This gives us an idea where the most influential peaks are located vs bg noise
    """
    __create_file(f'{path}density/')
    name = "50" if i == 0 else "100" if i == 1 else "200" if i == 2 else "all"
    load_name = f'top_{name}' if i != 3 else name
    if os.path.exists(f'{path}density/{bins}-{name}.png'):
        return
    data = read(f'{path}plotdata/{load_name}.txt')
    # data = data[:1000000]
    print(f'bins: {bins} | data: {load_name} | Status: ~')
    data1 = [f for f in data if f < 2000]
    fig, ax = plt.subplots(figsize=(4, 3.8))
    # Density
    sns.histplot(data1, stat='probability', ax=ax, bins=bins, kde=True, kde_kws={'bw_method': 0.05})
    plt.xlim(xmin=0, xmax=2000)
    ax.set(xlim=(0, 2000))
    ax.set_xlabel('m/z', fontsize=12)
    ax.set_ylabel('Density', fontsize=12)
    ax.tick_params(axis='x', labelsize=10)
    ax.tick_params(axis='y', labelsize=10)
    ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
    ax.set_title(f'{bins} Bins, Top {name} peaks', pad=10, fontsize=12, y=1.02)
    plt.savefig(f'{path}density/{bins}-{name}.png', bbox_inches='tight')
    plt.cla()
    return print(f'bins: {bins} | data: {load_name} | Status: ✓')


def cdf_plot(path):
    path='C:/Users/Tobias/'
    comb_freqs = {}
    for bins in [50, 100, 200, 'all']:
        print(f'CDF {bins}: ~', end='\r')
        file_name = f'top_{bins}.txt' if bins != 'all' else 'all.txt'
        if os.path.exists(f'{path}plotdata/top_{str(bins)}_cdf.txt'):
            data = read(f'{path}plotdata/top_{str(bins)}_cdf.txt')
        else:
            data = read(f'{path}plotdata/{file_name}')
            data = [f for f in data if 50 < f < 250]
            write(data, f'{path}plotdata/top_{str(bins)}_cdf.txt')
        data = sorted([round(f, 1) for f in data])
        b = get_aa_weights(data)
        freq_dict = {}
        fig, ax = plt.subplots(figsize=(4, 3.8))
        sns.ecdfplot(data, ax=ax)
        amount = len(data)
        minval = 0
        for x, xx in zip([f[0] for f in b], [f[1] for f in b]):
            count = 0
            for i, f in enumerate(data[minval:]):
                if f in x:
                    count += 1
                if f > x[2]:
                    yval = (minval + i) / amount
                    minval = i
                    freq_dict[xx] = f'{round(count / amount * 100, 2)}%'
                    break
            clr = 'b' if xx.endswith('a)') else 'r' if xx.endswith('b)') else 'g' if xx.endswith('y)') else 'm' if xx.endswith('c)') else 'orange'
            plt.vlines(x=x[1], ymin=0, ymax=yval, color=clr, linewidth=0.75, linestyles='dashed')
        with open(f'{path}plotdata/{str(bins)}_cdf_freq.txt', 'w') as file:
            json.dump(freq_dict, file)
        colors = ['b', 'r', 'g', 'm', 'orange']
        from matplotlib.lines import Line2D
        lines = [Line2D([0], [0], color=c, linewidth=2, linestyle='--') for c in colors]
        labels = ['a fragments', 'b fragments', 'y fragments', 'c fragments', 'z fragments']
        ax.set(xlim=(50, 250))
        ax.legend(lines, labels, prop=dict(size=10), loc='upper left')
        ax.set_title(f'Top {bins} peaks', pad=10, fontsize=12, y=1.02)
        ax.set_xlabel('m/z', fontsize=12)
        ax.set_ylabel('Proportion', fontsize=12)
        ax.tick_params(axis='x', labelsize=10)
        ax.tick_params(axis='y', labelsize=10)
        plt.savefig(f'{path}density/cdf_{bins}.png', bbox_inches='tight')
        print(f'CDF {bins}: Done')
        comb_freqs[bins] = freq_dict
        plt.cla()


def get_vals(file):
    data = read(file)
    a = []
    b = []
    c = []
    d = []
    for spectra in data:
        a.append([f[0] for f in spectra[:50]])
        b.append([f[0] for f in spectra[:100]])
        c.append([f[0] for f in spectra[:200]])
        d.append([f[0] for f in spectra])
    return a, b, c, d


def ms2_density_plot(path):
    if not (os.path.exists(f'{path}plotdata/top_50.txt') and os.path.exists(f'{path}plotdata/top_100.txt')
            and os.path.exists(f'{path}plotdata/top_200.txt') and os.path.exists(f'{path}plotdata/all.txt')):
        if not (len(sys.argv) > 1 and sys.argv[1] == '-test'):
            top_n_comp = list(Pool(min(16, len(glob(f'{path}data/*.txt')))).imap_unordered(get_vals, glob(f'{path}data/*.txt')))
            write(list(chain.from_iterable(chain.from_iterable([n[0] for n in top_n_comp]))), f'{path}plotdata/top_50.txt')
            write(list(chain.from_iterable(chain.from_iterable([n[1] for n in top_n_comp]))), f'{path}plotdata/top_100.txt')
            write(list(chain.from_iterable(chain.from_iterable([n[2] for n in top_n_comp]))), f'{path}plotdata/top_200.txt')
            write(list(chain.from_iterable(chain.from_iterable([n[3] for n in top_n_comp]))), f'{path}plotdata/all.txt')
    print('MS2 mz density plots                    ')
    bin_sizes = [50, 100, 200, 500]
    list(Pool(len(bin_sizes)).imap_unordered(partial(plot_topn, path, 0), bin_sizes))  # top 50 peaks
    list(Pool(len(bin_sizes)).imap_unordered(partial(plot_topn, path, 1), bin_sizes))  # top 100 peaks
    list(Pool(len(bin_sizes)).imap_unordered(partial(plot_topn, path, 2), bin_sizes))  # top 200 peaks
    list(Pool(len(bin_sizes)).imap_unordered(partial(plot_topn, path, 3), bin_sizes))  # top all peaks
    cdf_plot(path)


if __name__ == '__main__':
    path = sys.argv[1]
    ms2_density_plot(path)
