import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


def filter_box(dictionary, path):
    """
    Plots a boxplot of the search spaces included in an experiment.
    Each boxplot is the vector corresponding with a given percentile:
        Lower bound of the m/z search space.
        Upper bound of the m/z search space.
        Length of the m/z search space (Upper - Lower)

    Only applicable when comparing projects, as each project is very likely to use identical search spaces.
    Shows how the experiment setup will dictate what proteins and peptides are found by the MS instrument, based on the search space applied.
    """
    min_vals = dictionary['ms1']['mz_filter_min']
    max_vals = dictionary['ms1']['mz_filter_max']

    a = []
    for f in range(len(min_vals)):
        a.append([min_vals[f], 'Lower Bound'])
        a.append([max_vals[f], 'Upper Bound'])
        a.append([max_vals[f] - min_vals[f], 'Filter length'])
    fig, ax = plt.subplots(1, figsize=(8, 5))
    df = pd.DataFrame(a, columns=['Value', 'Bound'])
    sns.violinplot(data=df, x='Bound', y='Value', ax=ax)
    plt.ylim(ymin=0)
    ax.set_title('Comparison of m/z search space filters', fontsize=12)
    ax.set_xlabel('Bounds', fontsize=15)
    ax.set_ylabel('m/z', fontsize=15)
    ax.tick_params(axis='x', labelsize=10)
    ax.tick_params(axis='y', labelsize=10)
    plt.gca().set_ylim(bottom=0)

    plt.savefig(f'{path}search_space.png', bbox_inches='tight')
    plt.cla()

# Mean files per project is 80, median is 28

# Of our 652 projects, 586 has one combined lower bound.
# 56 has 2 different lower bounds, 6 has 3 different lower bounds and 3 has 4 different lower bounds.
#   4 is the highest amount of different lower bounds.
#   These are the files with different lower bounds:
# [[350, 400], [300, 350], [350, 375], [250, 300], [300, 450], [300, 350], [375, 400], [300, 350], [350, 375],
#   [220, 400], [300, 350], [320, 400], [300, 375], [300, 350], [300, 350], [350, 400], [300, 350], [350, 500],
#   [350, 400], [370, 375], [300, 400], [300, 400], [300, 400], [300, 400], [300, 375], [300, 400], [300, 350],
#   [200, 300], [380, 400], [350, 375], [300, 350], [350, 400], [300, 400], [300, 400], [300, 375], [350, 375],
#   [325, 350], [350, 400], [300, 375], [300, 400], [350, 375], [300, 400], [300, 360], [350, 395], [350, 380],
#   [350, 400], [300, 400], [300, 380], [250, 300], [350, 380], [300, 400], [350, 400], [350, 375], [375, 800],
#   [380, 400], [350, 380]]
# [[350, 370, 375], [350, 375, 400], [300, 380, 400], [300, 350, 375], [300, 350, 400], [300, 375, 400]]
# [[350, 375, 380, 400], [197, 300, 400, 450], [300, 301, 908, 909]]

# Of our 652 projects, 562 has one combined upper bound.
#   72 has 2 different upper bounds, 13 has 3 different upper bounds and 5 has four different upper bounds.
#   4 is the highest amount of different upper bounds.
#   These are the files with different upper bounds:
# [[1000, 1400], [1650, 1750], [1600, 1650], [1650, 1700], [1600, 2000], [1500, 2000], [1650, 2000], [1500, 1600],
#   [1750, 2000], [1700, 1800], [1400, 1575], [1400, 1500], [1400, 1500], [1500, 2200], [1500, 1750], [1600, 2000],
#   [1650, 1750], [1500, 1600], [1500, 1600], [1600, 1700], [1650, 1750], [1450, 1600], [1700, 1750], [1600, 1800],
#   [1600, 1800], [1650, 1700], [1650, 2000], [1650, 2000], [1600, 1800], [1800, 2000], [1500, 2000], [1600, 1750],
#   [1700, 2000], [1300, 1800], [1500, 2000], [1750, 2000], [1700, 1800], [1600, 1750], [1200, 1250], [1650, 1750],
#   [1600, 1650], [1500, 1650], [1600, 1700], [1400, 1800], [1400, 1600], [1600, 1800], [1500, 1600], [1700, 1800],
#   [1400, 1650], [1750, 1800], [1200, 1750], [1575, 1600], [1750, 2000], [1750, 1800], [1550, 2000], [1500, 2000],
#   [1500, 1600], [1500, 1650], [1700, 1800], [1500, 1580], [1650, 2000], [1800, 2000], [1650, 1750], [1200, 1400],
#   [1800, 2000], [1650, 1750], [600, 1200], [1500, 1600], [1600, 2000], [1700, 2000], [1650, 1700]]
# [[1700, 1800, 2000], [1400, 1500, 1600], [1500, 1550, 1700], [1700, 1750, 1800], [1300, 1600, 2000], [1200, 1700, 1750],
#   [1350, 1400, 1500], [1500, 1700, 2000], [1200, 1750, 2000], [1200, 1700, 1750], [1400, 1500, 1800], [1300, 1500, 1700], [1600, 1800, 1900]]
# [[1400, 1600, 1650, 1700], [850, 900, 1500, 1800], [1450, 1500, 1650, 1750], [1000, 1006, 1120, 1650], [1500, 1575, 1650, 2000]]

# These are the bounds where a single project has more than one lower and upper bound (45 files)
# [[[350, 400], [1000, 1400]], [[300, 350], [1650, 1750]], [[350, 375], [1500, 1600]], [[250, 300], [1700, 1800, 2000]],
#   [[350, 370, 375], [1750, 2000]], [[350, 375, 400], [1400, 1500, 1600]], [[300, 450], [1700, 1800]],
#   [[300, 350], [1500, 1550, 1700]], [[375, 400], [1400, 1575]], [[350, 375], [1400, 1500]], [[350, 375, 380, 400], [1400, 1500]],
#   [[220, 400], [1500, 2200]], [[300, 375], [1500, 1750]], [[300, 350], [1700, 1750, 1800]], [[300, 380, 400], [1300, 1600, 2000]],
#   [[350, 400], [1500, 1600]], [[300, 350], [1400, 1600, 1650, 1700]], [[197, 300, 400, 450], [850, 900, 1500, 1800]],
#   [[350, 500], [1350, 1400, 1500]], [[300, 350, 375], [1500, 1700, 2000]], [[300, 350, 400], [1450, 1500, 1650, 1750]],
#   [[300, 400], [1200, 1750, 2000]], [[300, 400], [1650, 2000]], [[300, 400], [1400, 1500, 1800]], [[200, 300], [1500, 2000]],
#   [[350, 375], [1750, 2000]], [[350, 400], [1700, 1800]], [[300, 400], [1500, 1650]], [[300, 400], [1600, 1700]],
#   [[300, 375], [1400, 1800]], [[350, 375], [1400, 1600]], [[300, 301, 908, 909], [1000, 1006, 1120, 1650]],
#   [[325, 350], [1300, 1500, 1700]], [[300, 375], [1400, 1650]], [[300, 400], [1200, 1750]], [[350, 375], [1575, 1600]],
#   [[350, 395], [1550, 2000]], [[350, 380], [1500, 2000]], [[350, 400], [1500, 1600]], [[300, 400], [1500, 1650]],
#   [[300, 380], [1700, 1800]], [[350, 380], [1500, 1580]], [[300, 375, 400], [1500, 1575, 1650, 2000]],
#   [[375, 800], [600, 1200]], [[380, 400], [1500, 1600]]]
# 28/45 has exactly 2 lower and upper bounds, 3/45 has exactly 3 lower and upper bounds, 2/45 has exactly 4 lower and upper bounds
# 44 files has exactly one lower bound and more than one upper bound and 20 files has exactly one upper bound and more than one lower bound.
# These were all taken from the mzML file filter and not from min/max values in the data
