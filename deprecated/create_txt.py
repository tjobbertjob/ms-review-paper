import sys

from pymongo import MongoClient

if len(sys.argv) == 1:
    path = input('Input path: ')
else:
    path = sys.argv[1]

projects = MongoClient()['reviewpaper']['project']
mzmls = MongoClient()['reviewpaper']['mzmlfiles']

combined_info = [f"project: {f['_id']} | " \
     f"species: {','.join(f['species'])} | " \
     f"tissue type: {','.join(f['tissues'])} | " \
     f"files: {', '.join([g['_id'].split('/')[4] for g in mzmls.find({'_id': {'$regex': f'.+{list(f.values())[0]}.+'}})])} " \
     f"link: f'https://www.ebi.ac.uk/pride/archive/projects/{list(f.values())[0]}/'"
     for f in projects.find()]

with open(f'{path}info.txt', 'w') as file:
    file.write('\n\n'.join(combined_info))

projects = projects.distinct('_id')
with open(f'{path}projects.txt', 'w') as file:
    file.write(', '.join(projects))

files = ['/'.join(f.split('/')[3:5]) for f in mzmls.distinct('_id')]
with open(f'{path}files.txt', 'w') as file:
    file.write(', '.join(files))
