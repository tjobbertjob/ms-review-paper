import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib import gridspec
from matplotlib.ticker import ScalarFormatter


def percentile_plot(path):
    df = pd.concat(
        [pd.read_csv(f, index_col=False) for f in [f'{path}plotdata/spectra.csv', f'{path}plotdata/file.csv', f'{path}plotdata/comp.csv']])
    df = df[df['Percentile'].isin(['0', '20', '40', '60', '80', '100'])]
    df2 = pd.concat([pd.read_csv(f, index_col=False) for f in
                     [f'{path}plotdata/spectra_mvm.csv', f'{path}plotdata/file_mvm.csv', f'{path}plotdata/comp_mvm.csv']])
    # plt.hist(df.loc[(df["MS level"] == 'ms1') & (df["Data type"] == 'mz') & (df["Percentile"] == 100) & (df["Data"] == 'file')]['Value'], bins=25); plt.show()  # Distribution checker

    log_mz = True
    for ms_level in ['ms1', 'ms2']:
        for f in ['mz', 'int']:
            # Create DFs
            plot_df1 = df.loc[(df["Data type"] == f) & (df["MS level"] == ms_level)]
            plot_df2 = df2.loc[(df2["Data type"] == f) & (df2["MS level"] == ms_level)]

            # Setup Figure
            plt.figure(figsize=(20, 10))
            gs = gridspec.GridSpec(1, 2, width_ratios=[3.5, 1])
            gs.update(wspace=0.1, hspace=0.1)

            # Percentile plot
            axes0 = plt.subplot(gs[0])
            if log_mz and f == 'mz':
                axes0.set_yscale("log")
                axes0.yaxis.set_minor_formatter(ScalarFormatter())
            sns.violinplot(data=plot_df1, x='Percentile', y='Value', hue='Data')
            L = plt.legend(loc="upper left")
            L.get_texts()[0].set_text('Projects')
            L.get_texts()[1].set_text('Files')
            L.get_texts()[2].set_text('Spectra')
            plt.ylabel(f'{f}')
            plt.xlabel(f'Percentiles')

            # MvM subplot
            axes0.set_title(f'{ms_level.upper()} analysis of {f} percentiles across spectra, files, and projects')
            axes1 = plt.subplot(gs[1])
            sns.violinplot(ax=axes1, data=plot_df2, x='MorM', y='Value', hue='Data')
            L = plt.legend(loc="upper left")
            L.get_texts()[0].set_text('Projects')
            L.get_texts()[1].set_text('Files')
            L.get_texts()[2].set_text('Spectra')
            plt.ylabel(f'{f}')
            plt.xlabel(f'Mean vs Median')
            axes1.set_title(f'Means vs medians of {ms_level.upper()} {f}')
            # plt.show()
            plt.savefig(f'{path}/violin_{f}_{ms_level}.png', format='png', bbox_inches='tight')
