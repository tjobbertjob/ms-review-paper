import re
import sys
from collections import defaultdict
from multiprocessing.pool import Pool

import numpy as np
import pandas as pd
from msgpack import unpackb, packb
from pymongo import MongoClient

from ms2_density import ms2_density_plot
from percentile_plot import percentile_plot
from rt_plots import rt_plots
from mz_filter import filter_box


def __from_byte(obj):
    return unpackb(obj, raw=False)


def __to_byte(obj):
    return packb(obj)


def read(name):
    with open(name, 'rb') as file:
        return __from_byte(file.read())


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(__to_byte(obj))


def percentile_write_data(dictionary, folder):
    a = [[f, g.split('_')[0], h, l, folder] for f in dictionary if isinstance(dictionary[f], dict)
         for g in dictionary[f] if isinstance(dictionary[f][g], dict) for h in dictionary[f][g] for l in
         dictionary[f][g][h]]
    df = pd.DataFrame(a, columns=['MS level', 'Data type', 'Percentile', 'Value', 'Data'])
    df.to_csv(f'{path}plotdata/{folder}.csv')


def MvM_write_data(dictionary, folder):
    a = [[f, g, l, 'Mean', folder] for f in dictionary if isinstance(dictionary[f], dict) for g in ['mz', 'int'] for l
         in dictionary[f][f'{g}_means']]
    b = [[f, g, l, 'Median', folder] for f in dictionary if isinstance(dictionary[f], dict) for g in ['mz', 'int'] for l
         in dictionary[f][f'{g}_percentiles']['50']]
    c = a + b
    df = pd.DataFrame(c, columns=['MS level', 'Data type', 'Value', 'MorM', 'Data'])
    df.to_csv(f'{path}plotdata/{folder}_mvm.csv')


def normalize_info(info_dict):
    def round_or_log(round_list, g):
        return [round(np.log(max(0.0001, f)), 3) if g.startswith('int')
                else round(f, 6) if g.startswith('rt') else round(f, 3)
                for f in round_list]

    for h in ['ms1', 'ms2']:
        for g in info_dict[h]:
            if isinstance(info_dict[h][g], dict):
                for x in info_dict[h][g]:
                    info_dict[h][g][x] = round_or_log(info_dict[h][g][x], g)
            else:
                info_dict[h][g] = round_or_log(info_dict[h][g], g)
    return info_dict


def ext(f, project, analysisdb):
    file = f['_id'].split('/')[4]
    if analysisdb.find_one({'_id': f'{project}/{file}'}) is None:
        ms1 = unpackb(f['ms1'])
        ms2 = unpackb(f['ms2'])
        info_dict = {
            'ms1': {'mz_percentiles': defaultdict(list),
                    'int_percentiles': defaultdict(list),
                    'mz_means': [], 'mz_vars': [], 'int_means': [], 'int_vars': [],
                    'mz_filter_min': [], 'mz_filter_max': []},
            'ms2': {'mz_percentiles': defaultdict(list),
                    'int_percentiles': defaultdict(list),
                    'mz_means': [], 'mz_vars': [], 'int_means': [], 'int_vars': []}}
        for h in ['ms1', 'ms2']:
            data = eval(h)
            for g in data:
                info_dict[h]['mz_means'].append(data[g]['mz'][1])
                info_dict[h]['mz_vars'].append(data[g]['mz'][3])
                info_dict[h]['int_means'].append(data[g]['int'][1])
                info_dict[h]['int_vars'].append(data[g]['int'][3])
                for i, x in enumerate([str(f) for f in list(range(0, 110, 10))]):
                    info_dict[h]['mz_percentiles'][x].append(data[g]['mz'][0][i])
                    info_dict[h]['int_percentiles'][x].append(data[g]['int'][0][i])

        info_dict = normalize_info(info_dict)
        info_dict.update({'ms1': packb(info_dict['ms1']), 'ms2': packb(info_dict['ms2'])})
        info_dict.update({'_id': f'{project}/{file}', 'type': 'file'})
        analysisdb.insert_one(info_dict)

    else:
        if project == 'PXD019564' and analysisdb.find_one({'_id': f'{project}/{file}', 'type': 'file'}) is not None:

            info_dict = analysisdb.find_one({'_id': f'{project}/{file}', 'type': 'file'})
            for f in ['ms1', 'ms2']:
                info_dict[f] = unpackb(info_dict[f])

            percentile_write_data(info_dict, 'spectra')
            MvM_write_data(info_dict, 'spectra')


def lim(lim_files, project, analysisdb):
    info_dict = {
        'ms1': {'mz_percentiles': defaultdict(list), 'int_percentiles': defaultdict(list),
                'mz_means': [], 'mz_vars': [], 'int_means': [], 'int_vars': [],
                'mz_filter_min': [], 'mz_filter_max': [], 'rt_end': [], 'rt_amount': [], 'rt_stepsize': []},
        'ms2': {'mz_percentiles': defaultdict(list), 'int_percentiles': defaultdict(list),
                'mz_means': [], 'mz_vars': [], 'int_means': [], 'int_vars': []},
        'files': []}

    if analysisdb.find_one({'_id': project, 'type': 'project', 'files': {'$all': [f['_id'] for f in lim_files]}}) is None:
        analysisdb.delete_one({'_id': project})
        for f in lim_files:
            ms1 = f['ms1']
            ms2 = f['ms2']

            info_dict['ms1']['rt_end'].append(f['rt'][0][-1])
            info_dict['ms1']['rt_amount'].append(f['rt'][2])
            info_dict['ms1']['rt_stepsize'].append(f['rt'][0][-1] / f['rt'][2])
            try:
                filter_values = f['ms1_filter'][re.search('\[\d*.\d*\-\d*\.\d*\]', f['ms1_filter']).regs[0][0] + 1:
                                                re.search('\[\d*.\d*\-\d*\.\d*\]', f['ms1_filter']).regs[0][
                                                    1] - 1].split('-')
                info_dict['ms1']['mz_filter_min'].append(int(float(filter_values[0])))
                info_dict['ms1']['mz_filter_max'].append(int(float(filter_values[1])))
            except:
                pass

            for h in ['ms1', 'ms2']:
                data = eval(h)
                info_dict[h]['mz_means'].append(data['mz'][1])
                info_dict[h]['mz_vars'].append(data['mz'][3])
                info_dict[h]['int_means'].append(data['int'][1])
                info_dict[h]['int_vars'].append(data['int'][3])
                for i, x in enumerate([str(f) for f in list(range(0, 110, 10))]):
                    info_dict[h]['mz_percentiles'][x].append(data['mz'][0][i])
                    info_dict[h]['int_percentiles'][x].append(data['int'][0][i])

            info_dict['files'].append(f['_id'])

        info_dict = normalize_info(info_dict)
        if info_dict['ms1']['mz_means']:
            info_dict.update({'ms1': packb(info_dict['ms1']), 'ms2': packb(info_dict['ms2'])})
            info_dict.update({'_id': project, 'type': 'project'})
            analysisdb.insert_one(info_dict)

    if project == 'PXD010859' and analysisdb.find_one({'_id': project, 'type': 'project'}) is not None:

        info_dict = analysisdb.find_one(
            {'_id': project, 'type': 'project', 'files': {'$all': [f['_id'] for f in lim_files]}})
        for f in ['ms1', 'ms2']:
            info_dict[f] = unpackb(info_dict[f])

        percentile_write_data(info_dict, 'file')
        MvM_write_data(info_dict, 'file')


def analysis(files):
    client = MongoClient()
    project = files[0].split('/')[3]

    analysisdb = client['reviewpaper']['analysis']
    ext_files = list(client['reviewpaper'][extdb].find({'_id': {'$in': files}, 'status': {'$exists': False}}))
    lim_files = list(client['reviewpaper'][limdb].find({'_id': {'$in': files}}))

    for f in ext_files:
        ext(f, project, analysisdb)

    lim(lim_files, project, analysisdb)


def intra_project():
    client = MongoClient()
    db = client['reviewpaper']['analysis']
    if db.find_one({'_id': 'comparison'}) is None:
        data = list(db.find({'type': 'project'}))
        info_dict = {
            'ms1': {'mz_percentiles': defaultdict(list), 'int_percentiles': defaultdict(list),
                    'mz_means': [], 'mz_vars': [], 'int_means': [], 'int_vars': [],
                    'mz_filter_min': [], 'mz_filter_max': [], 'rt_end': [], 'rt_amount': [], 'rt_stepsize': []},
            'ms2': {'mz_percentiles': defaultdict(list), 'int_percentiles': defaultdict(list),
                    'mz_means': [], 'mz_vars': [], 'int_means': [], 'int_vars': []}}
        for f in data:
            for g in ['ms1', 'ms2']:
                unpacked = unpackb(f[g])
                for h in unpacked:
                    if isinstance(unpacked[h], dict):
                        for l in [str(f) for f in list(range(0, 110, 10))]:
                            if len(unpacked[h][l]) != 0:
                                info_dict[g][h][l].append(round(np.mean(unpacked[h][l]), 3))
                    else:
                        if len(unpacked[h]) != 0:
                            info_dict[g][h].append(round(np.mean(unpacked[h]), 3))
        info_dict.update({'_id': 'comparison'})
        db.insert_one(info_dict)
    else:
        info_dict = db.find_one({'_id': 'comparison'})

    # Write and save data
    percentile_write_data(info_dict, 'comp')
    MvM_write_data(info_dict, 'comp')
    # Create plots
    print('m/z filter plots')
    filter_box(info_dict, path)
    print('m/z and intensity percentile plots')
    percentile_plot(path)
    print('Retention time plots')
    rt_plots(path)
    print('MS2 m/z density plots plots')
    ms2_density_plot(path)


if __name__ == '__main__':
    client = MongoClient()
    projectdb = client['reviewpaper']['project']
    analysisdb = client['reviewpaper']['analysis']

    path = sys.argv[1]
    extdb = 'ext'
    limdb = 'mzmlfiles'

    if input('reset all data?') in ['y', 'yes']:
        print('removing all data')
        analysisdb.drop()
    print('Spectra and file comparison')
    with Pool(16) as ps:
        for i, _ in enumerate(ps.imap_unordered(analysis, sorted([f['files'] for f in projectdb.find()], key=lambda x: len(x), reverse=True))):
            print(f'{i}/{len(list(projectdb.find()))}', end='\r')
    print('Project comparison')
    if input('reset comparison data?') in ['y', 'yes']:
        print('removing comparions data')
        analysisdb.delete_one({'_id': 'comparison'})
    intra_project()

# sys inputs:
#     1: base path