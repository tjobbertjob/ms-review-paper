import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib.patches import Rectangle
from msgpack import unpackb
from pymongo import MongoClient


def get_binsize(data):
    Q1 = np.quantile(data, 0.25)
    Q3 = np.quantile(data, 0.75)
    IQR = Q3 - Q1

    cube = np.cbrt(len(data))
    print(2 * IQR / cube)


def rt_plots(path):
    """
    Plots a histogram of retention time informations:
        Total rt in minutes
        Total amount of spectra (or time steps)
        Time step size (Total rt / time steps)

    Only viable when comparing files in a large project or project themselves.
    Shows the differences in experiment setup, runtime and step-size is distributed across files and projects.
    """
    client = MongoClient()
    db = client['reviewpaper']['analysis']

    handles = [Rectangle((0, 0), 1, 1, color=c, ec="k", alpha=1 if c == 'blue' else 0.625) for c in ['blue', 'green', 'cyan', 'red']]
    labels = ["Project Comparison", "PXD010859", "PXD005354", "PXD010957"]

    info_dict = db.find_one({'_id': 'comparison'})
    color = 'blue'
    info_dict2 = db.find_one({'_id': 'PXD010859', 'type': 'project'})
    info_dict2['ms1'] = unpackb(info_dict2['ms1'])
    color2 = 'green'
    info_dict3 = db.find_one({'_id': 'PXD005354', 'type': 'project'})
    info_dict3['ms1'] = unpackb(info_dict3['ms1'])
    color3 = 'cyan'
    info_dict4 = db.find_one({'_id': 'PXD010957', 'type': 'project'})
    info_dict4['ms1'] = unpackb(info_dict4['ms1'])
    color4 = 'red'

    statistic = 'density'
    plt.rcParams['patch.edgecolor'] = 'none'
    fig, (ax1, ax2, ax3) = plt.subplots(3, figsize=(7.5, 8))
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    fig.suptitle(f'Comparison of retention time statistics', fontsize=16, y=1.01)
    fig.subplots_adjust(hspace=0.6)

    # 1st Plot
    length = 5
    x_lim = 320
    y_lim = 0.02

    # Setup Data
    data = [round(f) for f in info_dict['ms1']['rt_end']]
    sns.histplot(data, stat=statistic, ax=ax1, color=color, binwidth=length, binrange=(0, x_lim))
    data = [round(f) for f in info_dict2['ms1']['rt_end']]
    sns.histplot(data, stat=statistic, ax=ax1, color=color2, binwidth=length, binrange=(0, x_lim), alpha=0.625)
    data = [round(f) for f in info_dict3['ms1']['rt_end']]
    sns.histplot(data, stat=statistic, ax=ax1, color=color3, binwidth=length, binrange=(0, x_lim), alpha=0.625)
    data = [round(f) for f in info_dict4['ms1']['rt_end']]
    d = sns.histplot(data, stat=statistic, ax=ax1, color=color4, binwidth=length, binrange=(0, x_lim), alpha=0.625)

    # Setup Axis
    ax1.set_title('Total retention time', pad=15, fontsize=12)
    ax1.set_xlabel('Minutes', fontsize=11)
    ax1.set_ylabel(statistic, fontsize=11)
    ax1.tick_params(axis='x', labelsize=10)
    ax1.tick_params(axis='y', labelsize=10)
    ax1.set(xlim=(0, x_lim), ylim=(0, y_lim))
    ax1.legend(handles, labels, prop=dict(size=10))
    ax1.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
    # Add text to plot
    d = [[f.get_x(), f.get_height()] for f in d.patches if f.get_height() > y_lim]
    for f in d:
        all_vals = '/'.join([str(round(g[1], 1)) for g in d if g[0] == f[0]])
        ax1.text(x=f[0]+length/2, y=y_lim, s=all_vals, fontsize=10, ha='center', va='bottom')

    # 2nd Plot
    length = 2500
    x_lim = 150000
    y_lim = 0.0001

    # Setup Data
    data = [round(f) for f in info_dict['ms1']['rt_amount']]
    sns.histplot(data, stat=statistic, ax=ax2, color=color, binwidth=length, binrange=(0, x_lim))
    data = [round(f) for f in info_dict2['ms1']['rt_amount']]
    sns.histplot(data, stat=statistic, ax=ax2, color=color2, binwidth=length, binrange=(0, x_lim), alpha=0.625)
    data = [round(f) for f in info_dict3['ms1']['rt_amount']]
    sns.histplot(data, stat=statistic, ax=ax2, color=color3, binwidth=length, binrange=(0, x_lim), alpha=0.625)
    data = [round(f) for f in info_dict4['ms1']['rt_amount']]
    d = sns.histplot(data, stat=statistic, ax=ax2, color=color4, binwidth=length, binrange=(0, x_lim), alpha=0.625)

    # Setup Axis
    ax2.set_title('Total amount of RT Steps (Spectra)', pad=15, fontsize=12)
    ax2.set_xlabel('Spectra Count', fontsize=11)
    ax2.set_ylabel(statistic, fontsize=11)
    ax2.tick_params(axis='x', labelsize=10)
    ax2.tick_params(axis='y', labelsize=10)
    ax2.set(xlim=(0, x_lim), ylim=(0, y_lim))
    ax2.legend(handles, labels, prop=dict(size=10))
    ax2.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
    # Add text to plot
    d = [[f.get_x(), f.get_height()] for f in d.patches if f.get_height() > y_lim]
    for f in d:
        all_vals = '/'.join(["{:.1e}".format(g[1]) for g in d if g[0] == f[0]])
        ax2.text(x=f[0]+length/2, y=y_lim, s=all_vals, fontsize=10, ha='center', va='bottom')

    # 3rd Plot
    length = 0.001
    x_lim = 0.016
    y_lim = 300

    # Setup Data
    data = [round(f, 3) for f in info_dict['ms1']['rt_stepsize']]
    sns.histplot(data, stat=statistic, ax=ax3, color=color, binwidth=length, binrange=(0, x_lim))
    data = [round(f, 3) for f in info_dict2['ms1']['rt_stepsize']]
    sns.histplot(data, stat=statistic, ax=ax3, color=color2, binwidth=length, binrange=(0, x_lim), alpha=0.625)
    data = [round(f, 3) for f in info_dict3['ms1']['rt_stepsize']]
    sns.histplot(data, stat=statistic, ax=ax3, color=color3, binwidth=length, binrange=(0, x_lim), alpha=0.625)
    data = [round(f, 3) for f in info_dict4['ms1']['rt_stepsize']]
    d = sns.histplot(data, stat=statistic, ax=ax3, color=color4, binwidth=length, binrange=(0, x_lim), alpha=0.625)

    # Setup Axis
    ax3.set_title('Retention time Step Size', pad=15, fontsize=12)
    ax3.set_xlabel('Seconds', fontsize=11)
    ax3.set_ylabel(statistic, fontsize=11)
    ax3.tick_params(axis='x', labelsize=10)
    ax3.tick_params(axis='y', labelsize=10)
    ax3.legend(handles, labels, prop=dict(size=10))
    ax3.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
    ax3.set(xlim=(0, x_lim), ylim=(0, y_lim))
    # Add text to plot
    d = [[f.get_x(), f.get_height()] for f in d.patches if f.get_height() > y_lim]
    for f in d:
        all_vals = '/'.join(["{:.1e}".format(g[1]) for g in d if g[0] == f[0]])
        ax3.text(x=f[0]+length/2, y=y_lim, s=all_vals, fontsize=10, ha='center', va='bottom')

    plt.savefig(f'{path}rt_statistics.png', bbox_inches='tight')
    # plt.show()
    plt.cla()
