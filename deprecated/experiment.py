import re
from collections import Counter

import numpy as np
import pandas as pd
import msgpack_numpy as np_msg
from pymongo import MongoClient
from statsmodels.formula.api import ols


def __from_byte(obj):
    return np_msg.unpackb(obj, raw=False)


def __to_byte(obj):
    return np_msg.packb(obj)


def read(name):
    with open(name, 'rb') as file:
        return __from_byte(file.read())


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(__to_byte(obj))


def insnames(string):
    if 'LTQ' in string:
        return 'LTQ Orbitrap'
    if 'Orbitrap' in string:
        return 'Orbitrap'
    if 'Exactive' in string:
        return 'Q Exactive'
    return 'Other'


def expnames(string):
    if 'Shotgun' in string or 'Bottom-up' in string:
        return 'Shotgun Proteomics'
    if 'Affinity' in string:
        return 'Affinity based MS Proteomics'
    if 'Gel' in string:
        return 'Gel-based Experiment'
    if 'Top-down' in string:
        return 'Top-down Proteomics'
    return 'Other'


def spenames(string):
    if 'Homo' in string:
        return 'Human'
    if 'Mus' in string:
        return 'Mouse'
    if 'yeast' in string:
        return 'Yeast'
    if 'Rat' in string:
        return 'Rat'
    if 'coli' in string:
        return 'E. coli'
    return 'Other'


def tagnames(string):
    if string is None:
        return 'Other'
    if 'Biological' in string:
        return 'Biological'
    if 'Biomedical' in string:
        return 'Biomedical'
    if 'Tech' in string:
        return 'Technical'
    return 'Other'


def softnames(string):
    if 'Mascot Parser' in string:
        return 'Mascot Parser'
    if 'Matrix Science Mascot' in string:
        return 'Matrix Science Mascot'
    if 'Scaffold' in string:
        return 'Scaffold'
    return 'Other'


def top_or_none(skey, counter):
    key_list = [f[0] for f in counter]
    if skey in key_list:
        return skey
    else:
        return 'Other'


def unlist(data):
    try:
        return str(data[0]) if isinstance(data, list) and data else str(data)
    except:
        pass

take_mean = False

a = MongoClient()['reviewpaper']['medanal']
c = MongoClient()['ms2ai']['projects']
b = {}
min_amount_classes = 20
acc_query = {'accession': {'$in': a.distinct('_id')}}
ins_counter = [str(classes[0]) for classes in Counter([f['instruments'] for f in c.find(acc_query)]).most_common() if classes[1] > min_amount_classes]
tag_counter = [str(classes[0]) for classes in Counter([unlist(f['projectTags']) for f in c.find(acc_query)]).most_common() if classes[1] > min_amount_classes]
tag_counter = [f for f in tag_counter if f != '[]']
spe_counter = [str(classes[0]) for classes in Counter([f['organisms'] for f in c.find(acc_query)]).most_common() if classes[1] > min_amount_classes]
spe_part_counter = [str(classes[0]) for classes in Counter([f['organismParts'] for f in c.find(acc_query) if f['organismParts']]).most_common() if classes[1] > min_amount_classes]
ptm_counter = [str(classes[0]) for classes in Counter([f['identifiedPTMStrings'] for f in c.find(acc_query)]).most_common() if classes[1] > min_amount_classes]

for i, f in enumerate(a.find()):
    print(i, end='\r')
    metadata = c.find_one({'accession': f['_id']})
    b[f['_id']] = {'data': {
        'ms1_mz': f['values']['ms1']['mz_mean'][1 if take_mean else 0],
        'ms2_mz': f['values']['ms2']['mz_mean'][1 if take_mean else 0],
        'ms1_int': f['values']['ms1']['int_mean'][1 if take_mean else 0],
        'ms2_int': f['values']['ms2']['int_mean'][1 if take_mean else 0]},
        # 'instrument': insnames(unlist(metadata['instruments'])),
        'instrument': metadata['instruments'] if metadata['instruments'] and metadata['instruments'] in ins_counter else "Other",
        'species': metadata['organisms'] if metadata['organisms'] and metadata['organisms'] in spe_counter else "Other",
        'species_part': metadata['organismParts'] if metadata['organismParts'] and metadata['organismParts'] in spe_part_counter else "Other",
        # 'tag': tagnames(unlist(metadata['projectTags'])),
        'tag': unlist(metadata['projectTags']) if unlist(metadata['projectTags']) in tag_counter else "Other",
        'PTMs': metadata['identifiedPTMStrings'] if metadata['identifiedPTMStrings'] and metadata['identifiedPTMStrings'] in ptm_counter else "Other",
    }

# print(b)
for z in ['ms1_mz', 'ms2_mz', 'ms1_int', 'ms2_int']:
    labels = [int(b[g]['data'][z]) for g in b]
    # OLS for everything
    features = [[b[g]['instrument'], b[g]['species'], b[g]['species_part'], b[g]['tag'], b[g]['PTMs']] for g in b]
    df = pd.DataFrame([f + [g] for f, g in zip(features, labels)], columns=['Instrument', 'Species', 'Species_Part', 'Tags', 'PTMs', 'StandardDeviation'])
    n_col = len(df.columns)-1
    q = [Counter(df[f]) for f in df.columns[:n_col] for g in Counter(list(np.unique(f)))]
    reg = ols('StandardDeviation ~ C(Instrument, Treatment(reference="Q Exactive")) '
              '                  + C(Species, Treatment(reference="Homo sapiens (human)")) '
              '                  + C(Species_Part, Treatment(reference="Other")) '
              '                  + C(Tags, Treatment(reference="Biological")) '
              '                  + C(PTMs, Treatment(reference="monohydroxylated residue")) ', data=df).fit()
    print(reg.summary())
ax_list = list(reg.params.axes[0])
ref_values = [[z for z in list(np.unique(df[f])) if z not in [g[re.search(f, g).span()[1]+4:-1] for g in ax_list if f in g]] for f in list(df.columns)[:n_col]]
print(f'### Reference Classes {z} ###')
for f, g in zip(df.columns[:n_col], ref_values):
    print(f"{f}: {g[0]}")

for ff in ['instrument', 'species', 'species_part', 'tag', 'PTMs']:
    print(f'\n{ff}')
    for g in Counter([b[f][ff] for f in b]).most_common():
        print(f'{g[0]}: {g[1]}')
print()