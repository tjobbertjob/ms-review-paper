from collections import Counter
from itertools import chain
from multiprocessing import Pool

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure
from pymongo import MongoClient


def _get_dbs(db_name, *args):
    dbs = [MongoClient()[db_name][str(f)] for f in args]
    return dbs[0] if len(dbs) == 1 else dbs


def _gradient_length():
    files = _get_dbs('ms2ai', 'files')
    b = files.find({'gradient length': {'$exists': True}}).distinct('accession')
    c = {f: [round(g) for g in files.find({'accession': f}).distinct('gradient length')] for f in b}
    d = dict(sorted(c.items(), key=lambda x: len(x[1]), reverse=True))
    all_grads = list(chain(*(d.values())))
    unique_grad = [len(list(set(c[f]))) for f in c]

    plt.figure(figsize=(12, 5))
    plt.subplots_adjust(wspace=0.65)
    ax1 = plt.subplot2grid((1, 3), (0, 2), colspan=1, rowspan=1)
    ax1.set_xlim(1, 6)
    sns.histplot(unique_grad, stat='probability', binwidth=1, ax=ax1)
    ax1.set_xlabel("Unique Gradients per Project")

    ax2 = plt.subplot2grid((1, 3), (0, 0), colspan=2, rowspan=1)
    ax2.set_xlabel("Gradient Length (minutes)")
    ax2.set_xlim(0, 300)
    sns.histplot(all_grads, stat='probability', binwidth=7.5, ax=ax2)
    ax2 = ax2.twinx()
    sns.kdeplot(all_grads, cumulative=True, ax=ax2, color='red')
    plt.tight_layout()
    plt.savefig('G:/My Drive/Universitet/PhD/Papers/MS Data Review/Figures/Main/Figure 5.png')


def _mz_filter():
    files = _get_dbs('ms2ai', 'files')
    b = files.find({'ms1 filter max': {'$exists': True}}).distinct('accession')
    d = {f: {g['Raw file']: {'min': g['ms1 filter min'], 'max': g['ms1 filter max'], 'length': g['ms1 filter max'] - g['ms1 filter min']}
             for g in files.find({'ms1 filter max': {'$exists': True}, 'accession': f}) if not isinstance(g['ms1 filter max'], list)} for f in b}

    all_filts = list(chain(*[list(f.values()) for f in list(d.values())]))
    unique_filts = [len(set((d[f][g]['min'], d[f][g]['max']) for g in d[f])) for f in d]
    counted_filts = Counter(tuple(item) for item in [[d[f][g]['min'], d[f][g]['max']] for f in d for g in d[f]]).most_common()
    fig = plt.figure(figsize=(12, 5))
    plt.subplots_adjust(wspace=0.65)
    ax1 = plt.subplot2grid((1, 4), (0, 0), colspan=1, rowspan=1)
    sns.violinplot(data=[g['min'] for g in all_filts], ax=ax1, color='#4285f4', bw=0.2)
    ax1.set_xticklabels(['Lower bound'], fontsize=10)

    ax2 = plt.subplot2grid((1, 4), (0, 1), colspan=1, rowspan=1)
    sns.violinplot(data=[g['max'] for g in all_filts], ax=ax2, color='#ea4335', bw=0.2)
    ax2.set_xticklabels(['Upper bound'], fontsize=10)

    ax3 = plt.subplot2grid((1, 4), (0, 2), colspan=1, rowspan=1)
    sns.violinplot(data=[g['length'] for g in all_filts], ax=ax3, color='#bf9000', bw=0.2)
    ax3.set_xticklabels(['Bound length'], fontsize=10)

    ax4 = plt.subplot2grid((1, 4), (0, 3), colspan=1, rowspan=1)
    sns.histplot(unique_filts, stat='probability', binwidth=1, ax=ax4)
    ax4.set_xlabel("Unique Filters")
    ax4.set_xlim(1, 6)
    plt.xticks(np.arange(1, 6, 1))
    plt.show()

    plt.tight_layout()
    plt.savefig('d:/data/metadata/review/filter.png')


def _ms1_resolution():
    files = _get_dbs('ms2ai', 'files')
    data = [round(f['ms1 scans'] / f['gradient length'], 2) for f in files.find({'ms1 scans': {'$exists': True}})]

    fig, ax = plt.subplots(figsize=(8, 5))
    sns.histplot(data, stat='probability', binwidth=7.5, ax=ax)
    ax2 = ax.twinx()
    sns.kdeplot(data, cumulative=True, ax=ax2, color='red')
    plt.tight_layout()
    ax.set_xlim(0, 250)
    plt.show()


if __name__ == '__main__':
    files = _get_dbs('ms2ai', 'files')
    _gradient_length()
    _mz_filter()
