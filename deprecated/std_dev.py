import sys
from collections import defaultdict
from functools import partial
from itertools import chain
from multiprocessing.pool import Pool

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib.lines import Line2D
from msgpack import unpackb, packb
from pymongo import MongoClient


def __from_byte(obj):
    return unpackb(obj, raw=False)


def __to_byte(obj):
    return packb(obj)


def read(name):
    with open(name, 'rb') as file:
        return __from_byte(file.read())


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(__to_byte(obj))


def outlier_analysis(data, ms_level):
    client = MongoClient()
    int_median = np.std(list(chain.from_iterable([j['values'][ms_level][f'int_median'][2] for j in client['reviewpaper']['medanal'].find()])))
    int_mean = np.std(list(chain.from_iterable([j['values'][ms_level][f'int_mean'][2] for j in client['reviewpaper']['medanal'].find()])))
    mz_median = np.std(list(chain.from_iterable([j['values'][ms_level][f'mz_median'][2] for j in client['reviewpaper']['medanal'].find()])))
    mz_mean = np.std(list(chain.from_iterable([j['values'][ms_level][f'mz_mean'][2] for j in client['reviewpaper']['medanal'].find()])))

    a = [f[0] for f in sorted([[f['_id'], f['values'][ms_level]['mz_median'][0]] for f in data], key=lambda x: x[1], reverse=True) if f[1] > mz_median]
    a1 = sorted([[f['_id'], f['values'][ms_level]['mz_median'][0], len(f['values'][ms_level]['mz_median'][2])] for f in data], key=lambda x: x[1], reverse=True)

    b = [f[0] for f in sorted([[f['_id'], f['values'][ms_level]['mz_mean'][0]] for f in data], key=lambda x: x[1], reverse=True) if f[1] > mz_mean]
    b1 = sorted([[f['_id'], f['values'][ms_level]['mz_median'][0], len(f['values'][ms_level]['mz_mean'][2])] for f in data], key=lambda x: x[1], reverse=True)

    c = [f[0] for f in sorted([[f['_id'], f['values'][ms_level]['int_median'][0]] for f in data], key=lambda x: x[1], reverse=True) if f[1] > int_median]
    c1 = sorted([[f['_id'], f['values'][ms_level]['mz_median'][0], len(f['values'][ms_level]['int_median'][2])] for f in data], key=lambda x: x[1], reverse=True)

    d = [f[0] for f in sorted([[f['_id'], f['values'][ms_level]['int_mean'][0]] for f in data], key=lambda x: x[1], reverse=True) if f[1] > int_mean]
    d1 = sorted([[f['_id'], f['values'][ms_level]['mz_median'][0], len(f['values'][ms_level]['int_mean'][2])] for f in data], key=lambda x: x[1], reverse=True)

    a2 = [item in a for item in b]
    b2 = [item in c for item in d]
    c2 = [item in a for item in c]
    d2 = [item in b for item in d]

    pride = MongoClient()['ms2ai']['pride']
    a3 = [pride.find_one({'accession': f}) for f in a]
    b3 = [pride.find_one({'accession': f}) for f in b]
    c3 = [pride.find_one({'accession': f}) for f in c]
    d3 = [pride.find_one({'accession': f}) for f in d]
    return [a, b, c, d, a1, b1, c1, d1, a2, b2, c2, d2, a3, b3, c3, d3]


def analysis(min_files_amount, files):
    if len(sys.argv) > 1 and sys.argv[1] == '-test':
        extdb = 'copyext'
        limdb = 'copylim'
    else:
        extdb = 'ext'
        limdb = 'mzmlfiles'
    client = MongoClient()
    project = files[0].split('/')[3]

    ext_files = list(client['reviewpaper'][extdb].find({'_id': {'$in': files}, 'status': {'$exists': False}}))
    lim_files = list(client['reviewpaper'][limdb].find({'_id': {'$in': files}}))

    base = defaultdict(list)
    try:
        if len(lim_files) >= min_files_amount:
            for f in lim_files:
                base['ms1_mz_mean'].append(f['ms1']['mz'][1])
                base['ms1_mz_median'].append(f['ms1']['mz'][0][6])
                base['ms1_int_mean'].append(f['ms1']['int'][1])
                base['ms1_int_median'].append(f['ms1']['int'][0][6])
                base['ms2_mz_mean'].append(f['ms2']['mz'][1])
                base['ms2_mz_median'].append(f['ms2']['mz'][0][6])
                base['ms2_int_mean'].append(f['ms2']['int'][1])
                base['ms2_int_median'].append(f['ms2']['int'][0][6])

            if base != defaultdict(list):
                output = {'ms1': {'mz_mean': [np.std(base['ms1_mz_mean']), np.mean(base['ms1_mz_mean']), base['ms1_mz_mean']],
                                  'mz_median': [np.std(base['ms1_mz_median']), np.mean(base['ms1_mz_median']), base['ms1_mz_median']],
                                  'int_mean': [np.std(base['ms1_int_mean']), np.mean(base['ms1_int_mean']), base['ms1_int_mean']],
                                  'int_median': [np.std(base['ms1_int_median']), np.mean(base['ms1_int_median']), base['ms1_int_median']]},
                          'ms2': {'mz_mean': [np.std(base['ms2_mz_mean']), np.mean(base['ms2_mz_mean']), base['ms2_mz_mean']],
                                  'mz_median': [np.std(base['ms2_mz_median']), np.mean(base['ms2_mz_median']), base['ms2_mz_median']],
                                  'int_mean': [np.std(base['ms2_int_mean']), np.mean(base['ms2_int_mean']), base['ms2_int_mean']],
                                  'int_median': [np.std(base['ms2_int_median']), np.mean(base['ms2_int_median']), base['ms2_int_median']]}}
                client['reviewpaper']['medanal'].insert_one({'_id': project, 'values': output})
    except:
        print(lim_files)


if __name__ == '__main__':
    client = MongoClient()
    projectdb = client['reviewpaper']['project']
    analysisdb = client['reviewpaper']['analysis']

    if len(sys.argv) > 1 and sys.argv[1] == '-test':
        path = '/Users/tobias/Dropbox/Universitet/PhD/ms2ai/Data/plots/'
        extdb = 'copyext'
        limdb = 'copylim'
    else:
        path = '/data/tobias/plots/'
        extdb = 'ext'
        limdb = 'mzmlfiles'
    min_files_amount = 2
    print('Spectra and file comparison')
    if input('reset?') in ['yes', 'y']:
        client['reviewpaper']['medanal'].drop()
        with Pool(16) as ps:
            for i, _ in enumerate(ps.imap_unordered(partial(analysis, min_files_amount), sorted([f['files'] for f in projectdb.find()], key=lambda x: len(x), reverse=True))):
                print(f'{i}/{len(list(projectdb.find()))}', end='\r')
    data = list(client['reviewpaper']['medanal'].find())

    A = outlier_analysis(data, 'ms1')
    B = outlier_analysis(data, 'ms2')

    print(f'Total projects with >{min_files_amount} files: {len(data)} ')
    # for f in ['ms1', 'ms2']:
    for g in ['mz', 'int']:
        f = 'ms1'
        ff = 'ms2'
        ms1_std_dist_median = [j['values'][f][f'{g}_median'][0] for j in client['reviewpaper']['medanal'].find()]
        ms1_prjtwide_median_all = np.std(list(chain.from_iterable([j['values'][f][f'{g}_median'][2] for j in client['reviewpaper']['medanal'].find()])))  # All file medians
        ms1_std_dist_mean = [j['values'][f][f'{g}_mean'][0] for j in client['reviewpaper']['medanal'].find()]
        ms1_prjtwide_mean_all = np.std(list(chain.from_iterable([j['values'][f][f'{g}_mean'][2] for j in client['reviewpaper']['medanal'].find()])))  # All file means

        ms2_std_dist_median = [j['values'][ff][f'{g}_median'][0] for j in client['reviewpaper']['medanal'].find()]
        ms2_prjtwide_median_all = np.std(list(chain.from_iterable([j['values'][ff][f'{g}_median'][2] for j in client['reviewpaper']['medanal'].find()])))  # All file medians
        ms2_std_dist_mean = [j['values'][ff][f'{g}_mean'][0] for j in client['reviewpaper']['medanal'].find()]
        ms2_prjtwide_mean_all = np.std(list(chain.from_iterable([j['values'][ff][f'{g}_mean'][2] for j in client['reviewpaper']['medanal'].find()])))  # All file means
        plt.rcParams.update({'font.size': 12})

        # Setup plot
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 3.5))
        fig.suptitle(f'Comparison of standard deviation between projects means and all files', fontsize=14, y=1.05)
        fig.subplots_adjust(wspace=0.25)
        plt.xlabel('Standard deviation')
        ax1.ticklabel_format(style='plain')
        ax2.ticklabel_format(style='plain')
        if g == 'int':
            ax1.set(xlim=(0, 1.1 * max(ms1_prjtwide_median_all, ms1_prjtwide_mean_all)))
            ax2.set(xlim=(0, 1.1 * max(ms2_prjtwide_median_all, ms2_prjtwide_mean_all)))
        else:
            ax1.set(xlim=(0, (1 * max(max(ms1_std_dist_median), max(ms1_std_dist_mean)))))
            ax2.set(xlim=(0, (1 * max(max(ms2_std_dist_median), max(ms2_std_dist_mean)))))

        # Add data
        a = sns.kdeplot(ms1_std_dist_median, bw_method=0.2, color='blue', ax=ax1)
        b = sns.kdeplot(ms1_std_dist_mean, bw_method=0.2, color='green', ax=ax1)
        vline_height = max(a.viewLim.bounds[3], b.viewLim.bounds[3])
        ax1.vlines(x=ms1_prjtwide_median_all, ymin=0, ymax=vline_height, linewidth=1.25, linestyles='dashed',
                   color='blue')
        ax1.vlines(x=ms1_prjtwide_mean_all, ymin=0, ymax=vline_height, linewidth=1.25, linestyles='dashed',
                   color='green')

        a = sns.kdeplot(ms2_std_dist_median, bw_method=0.2, color='blue', ax=ax2)
        b = sns.kdeplot(ms2_std_dist_mean, bw_method=0.2, color='green', ax=ax2)
        vline_height = max(a.viewLim.bounds[3], b.viewLim.bounds[3])
        ax2.vlines(x=ms2_prjtwide_median_all, ymin=0, ymax=vline_height, linewidth=1.25, linestyles='dashed',
                   color='blue')
        ax2.vlines(x=ms2_prjtwide_mean_all, ymin=0, ymax=vline_height, linewidth=1.25, linestyles='dashed',
                   color='green')

        # Legends
        lines = [Line2D([0], [0], color=c, linewidth=2, linestyle=d) for c in ['blue', 'green'] for d in
                 ['solid', 'dashed']]
        labels = ['Within Project (Median)', 'All Files (Median)', 'Within Project (Mean)', 'All Files (Mean)']
        ax1.legend(lines, labels, prop=dict(size=10))
        ax2.legend(lines, labels, prop=dict(size=10))
        ax1.tick_params(axis='x', labelsize=10)
        ax1.tick_params(axis='y', labelsize=10)
        ax2.tick_params(axis='x', labelsize=10)
        ax2.tick_params(axis='y', labelsize=10)
        ax1.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        ax2.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        if g == 'int':
            ax1.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
            ax2.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))

        ax1.set_title(f'{f.upper()} {g.replace("int", "intensity").replace("mz", "m/z")}', pad=10, fontsize=13)
        ax1.set_xlabel('Standard Deviation', fontsize=12)
        ax1.set_ylabel('Density', fontsize=12)
        ax2.set_title(f'{ff.upper()} {g.replace("int", "intensity").replace("mz", "m/z")}', pad=10, fontsize=13)
        ax2.set_xlabel('Standard Deviation', fontsize=12)
        ax2.set_ylabel('Density', fontsize=12)
        plt.savefig(f'{path}{g}_stddev_{min_files_amount}.png', bbox_inches='tight')
        # plt.show()
        plt.cla()

# /mnt/c/Users/tobias/mongodb-tools/bin/mongorestore.exe --db reviewpaper reviewpaper/
# /mnt/c/Users/tobias/mongodb-tools/bin/mongorestore.exe --gzip --archive=project.bson.gz --db reviewpaper
