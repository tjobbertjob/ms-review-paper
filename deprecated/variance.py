from collections import defaultdict
from itertools import chain
import seaborn as sns
import numpy as np
from matplotlib import pyplot as plt
from pymongo import MongoClient

if __name__ == '__main__':
    client = MongoClient()
    projectdb = client['reviewpaper']['project']
    analysisdb = client['reviewpaper']['mzmlfiles']
    file_vars = defaultdict(list)
    [file_vars[f['_id'].split('/')[3]].append([f[g][gg][3] for g in ['ms1', 'ms2'] for gg in ['mz', 'int']]) for f in analysisdb.find()]
    project_summary = [[np.mean([gg[g] for gg in f]) for g in range(4)] for f in file_vars.values()]
    project_vars = {}
    [project_vars.update({f: {'ms1 m/z': ff[0], 'ms1 int': ff[1], 'ms2 m/z': ff[2], 'ms2 int': ff[3]}}) for f, ff in zip(file_vars.keys(), project_summary)]
    global_vars = [np.mean([g[f] for g in list(chain(*file_vars.values()))]) for f in range(4)]

    i = 0
    for ff in ['ms1', 'ms2']:
        for g in ['m/z', 'int']:
            values = np.log([f[f'{ff} {g}'] for f in project_vars.values()]) if g == 'int' else [f[f'{ff} {g}'] for f in project_vars.values()]
            global_var = np.log(global_vars[i]) if g == 'int' else global_vars[i]
            a = sns.kdeplot(values, bw_method=0.2, color='blue')
            vline_height = a.viewLim.bounds[3]
            # plt.xlim([a.viewLim.bounds[1], a.viewLim.bounds[2]*(1 if g == 'int' else 1)])
            plt.vlines(x=global_var, ymin=0, ymax=vline_height, linewidth=1.25, linestyles='dashed', color='blue')
            plt.title(f'{ff}{g}')
            plt.show()
            i += 1

    print()