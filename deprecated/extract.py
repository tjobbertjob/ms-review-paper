import glob
import gzip
import random
import sys
from functools import partial
from itertools import chain
from multiprocessing import Pool
from time import time

import msgpack
import numpy as np
from pymongo import MongoClient
from pyteomics import mzml


def __to_byte(obj):
    return msgpack.packb(obj)


def write(obj, name):
    with open(name, 'wb') as file:
        return file.write(__to_byte(obj))


def __validated_input(prompt, valid_values, show_options=True):
    value = input(prompt + ' | ' + ' / '.join(valid_values) + "\n") if show_options else input(prompt)
    while value not in valid_values:
        value = input(prompt + ' | ' + ' / '.join(valid_values) + "\n") if show_options else input(prompt)
    return value


def __process_ms(spectrum):
    scan_info = spectrum['scanList']
    scan_time = scan_info['scan'][0]['scan start time']
    filter_string = scan_info['scan'][0]['filter string']
    mz = spectrum['m/z array']
    int = spectrum['intensity array']
    return {'rt': scan_time, 'int': int.tolist(), 'mz': mz.tolist()}, filter_string


def postcalcs(info_list):
    percentiles = [np.percentile(info_list, f) for f in range(0, 110, 10)]
    mean = np.mean(info_list)
    amount = len(info_list)
    variation = np.var(info_list)

    return percentiles, mean, amount, variation


def pridedata(pridedb, project):
    # project = 'PXD008875'
    pride_entry = pridedb.find_one({'accession': project})
    list_of_interesting_keys = ['species', 'tissues', 'instrumentNames', 'projectTags', 'reanalysis', 'experimentTypes',
                                'sampleProcessingProtocol', 'dataProcessingProtocol', 'numProteins', 'numPeptides',
                                'numSpectra', 'numUniquePeptides']
    interesting_dict = {}
    [interesting_dict.update({f: pride_entry[f]}) for f in list_of_interesting_keys]
    return interesting_dict


def mzML_statistics(mzmlfile, path, multi=False, extended=False):
    try:
        client = MongoClient()
        extmzmldb = client['reviewpaper']['ext']
        mzmldb = client['reviewpaper']['mzmlfiles']
        projectdb = client['reviewpaper']['project']
        pridedb = client['ms2ai']['pride']

        if extended:
            if extmzmldb.find_one({'_id': mzmlfile}) is not None:
                return
        else:
            if mzmldb.find_one({'_id': mzmlfile}) is not None:
                return

        project = mzmlfile[len(path):len(path) + 9]
        with gzip.open(mzmlfile, 'rb') as gzipfile:
            data = mzml.MzML(gzipfile)

            rt = []
            ms1_mz = []
            ms2_mz = []
            ms1_int = []
            ms2_int = []
            ms2_dens = []
            spectra_data = {'ms1': {}, 'ms2': {}, 'rt': [], 'ms1_filter': []}


            for i, spectrum in enumerate(data):
                try:
                    if i % 1000 == 0 and not multi:
                        print(f'{i}/{len(data)}')
                    ms_spectrum, filter_string = __process_ms(spectrum)
                    rt.append(ms_spectrum['rt'])
                    if spectrum['ms level'] == 1:
                        ms1_filter_string = filter_string
                        if not extended:
                            ms1_mz.append(ms_spectrum['mz'])
                            ms1_int.append(ms_spectrum['int'])
                        else:
                            spectra_data['ms1'][str(i)] = {'mz': postcalcs(ms_spectrum['mz']),
                                                           'int': postcalcs(ms_spectrum['int'])}

                    elif spectrum['ms level'] == 2:
                        if not extended:
                            ms2_mz.append(ms_spectrum['mz'])
                            ms2_int.append(ms_spectrum['int'])
                        else:
                            spectra_data['ms2'][str(i)] = {'mz': postcalcs(ms_spectrum['mz']),
                                                           'int': postcalcs(ms_spectrum['int']),
                                                           'filter': filter_string}

                            ms2_dens.append(sorted([[i, ii] for i, ii in zip(ms_spectrum['mz'], ms_spectrum['int'])], key=lambda x: x[1], reverse=True))
                except:
                    pass

            if extended:
                spectra_data['rt'] = postcalcs(rt)
                spectra_data['ms1_filter'] = ms1_filter_string
                spectra_data['ms1'] = msgpack.packb(spectra_data['ms1'])
                spectra_data['ms2'] = msgpack.packb(spectra_data['ms2'])
                insert_data = {'_id': mzmlfile}
                insert_data.update(spectra_data)
                write(ms2_dens, f"{path}{'-'.join(mzmlfile.replace('/', '-').split('-')[-3:-1])}.txt")

                extmzmldb.insert_one(insert_data)

            else:
                ms1_dict = {'mz': postcalcs(list(chain.from_iterable(ms1_mz))),
                            'int': postcalcs(list(chain.from_iterable(ms1_int)))}
                ms2_dict = {'mz': postcalcs(list(chain.from_iterable(ms2_mz))),
                            'int': postcalcs(list(chain.from_iterable(ms2_int)))}

                # Insert mzML data in mzML DB
                mzml_data = {'_id': mzmlfile, 'ms1': ms1_dict, 'ms2': ms2_dict, 'rt': postcalcs(rt),
                             'ms1_filter': ms1_filter_string}
                mzmldb.insert_one(mzml_data)

                # Get data from PRIDE DB
                pride_project_data = pridedata(pridedb, project)

                # Insert or update project DB
                if projectdb.find_one({'_id': project}) is not None:
                    write_dict = projectdb.find_one({'_id': project})
                    write_dict['files'].append(str(mzmlfile))
                    projectdb.replace_one(projectdb.find_one({'_id': project}), write_dict)
                else:
                    write_dict = {'_id': project, 'files': [mzmlfile]}
                    write_dict.update(pride_project_data)
                    projectdb.insert_one(write_dict)
    except Exception as e:
        try:
            if extended:
                insert_data = {'_id': mzmlfile, 'status': 'too big'}
                extmzmldb.insert_one(insert_data)
        except:
            print('too_big failed')
            pass
        pass


def limited():
    start = time()
    print('Limited extractor')
    # Limited extractor
    poolsize = min(len(mzML_files), 8)
    with Pool(poolsize) as ps:
        for i, _ in enumerate(
                ps.imap_unordered(partial(mzML_statistics, path=path, multi=True, extended=False), mzML_files)):
            inter_stop = time()
            print(
                f'{i} / {len(mzML_files)}. Passed: {round(inter_stop - start)}sec/{round((inter_stop - start) / 60)}min, '
                f'Estimated finish time: {round((len(mzML_files) / (i + 1) * (inter_stop - start)) - (inter_stop - start))}sec'
                f'/{round((((len(mzML_files) / (i + 1) * (inter_stop - start)) - (inter_stop - start)) / 60))}min',
                end='\r')


def extended():
    start = time()
    print('Extended extractor')
    extmzmldb = MongoClient()['reviewpaper']['ext']
    mzML_files_spec = []
    for f in reversed(mzML_files):
        if extmzmldb.find_one({'_id': f}) is None:
            mzML_files_spec.append(f)
        if len(mzML_files_spec) > int(sys.argv[2]):
            break

    print(len(mzML_files_spec))
    poolsize = min(len(mzML_files_spec), 8)
    with Pool(poolsize) as ps:
        for i, _ in enumerate(
                ps.imap_unordered(partial(mzML_statistics, path=path, multi=True, extended=True), mzML_files_spec)):
            inter_stop = time()
            print(
                f'{i} / {len(mzML_files_spec)}. Passed: {round(inter_stop - start)}sec/{round((inter_stop - start) / 60)}min, '
                f'Estimated finish time: {round((len(mzML_files_spec) / (i + 1) * (inter_stop - start)) - (inter_stop - start))}sec'
                f'/{round((((len(mzML_files_spec) / (i + 1) * (inter_stop - start)) - (inter_stop - start)) / 60))}min.',
                end='\r')


def reset_dbs(lim=False, ext=False):
    client = MongoClient()
    db = client['reviewpaper']

    if lim:
        mzml_db = db['mzmlfiles']
        project_db = db['project']
        if mzml_db.find_one() is not None:
            reset = __validated_input('Reset limited DB?', ['y', 'n']) == 'y'
            if reset:
                mzml_db.drop()
                project_db.drop()

    if ext:
        extmzmldb = db['ext']
        if extmzmldb.find_one() is not None:
            reset = __validated_input('Reset extended DB?', ['y', 'n']) == 'y'
            if reset:
                extmzmldb.drop()


if __name__ == '__main__':
    path = sys.argv[3]
    print('Getting mzML files')
    mzML_files = glob.glob(f'{path}PXD*/*/*.mzML.gz')
    mzML_files = [f.replace('\\', '/') for f in mzML_files]
    random.seed(1)
    random.shuffle(mzML_files)
    print('Total .mzML.gz files: ', len(mzML_files))

    if len(sys.argv) == 1 or sys.argv[1] == 'both':
        reset_dbs(lim=True, ext=True)
        limited()
        extended()
    if sys.argv[1] in ['lim', 'limited']:
        reset_dbs(lim=True)
        limited()
    elif sys.argv[1] in ['ext', 'extended']:
        reset_dbs(ext=True)
        extended()
    else:
        print('no input')
        quit()

# sys inputs:
#     1: extended or limited extraction
#     2: amount to extract
#     3: base path