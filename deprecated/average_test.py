import os
import random
import shutil
import string
import sys
from glob import glob
from json import loads, dump

import pandas as pd
from pymongo import MongoClient

from extractor.common import _import_table
from storage.db_init import _get_dbs
from storage.utils import _get_path

db = sys.argv[1]
dbs = [f for f in MongoClient().list_database_names() if db in f]


def _random_name(model):
    characters = string.ascii_letters + string.digits
    return f"{model}-{''.join(random.choice(characters) for i in range(10))}"


def _save_params(path, params):
    params_file = loads(open(f'{path}metadata/parameters.txt', 'r').read()) if os.path.exists(f'{path}metadata/parameters.txt') else {}
    with open(f'{path}metadata/parameters.txt', 'w') as file:
        dump({**params_file, **{params['_id']: params}}, file, indent=4)


def _test_oob():
    csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/prosit-*.csv') if len(f.split('-')) == 2]
    for csv_file in csv_files:
        csv = _import_table(csv_file)
        model = csv[csv['type'] == 'train']['db'][0]
        if model == 'min_high':
            for f in ['min_low', 'min_low_below']:
                print(f'Model test on {f}')
                os.system(f'python network_api.py -tm {model} -ts 100 -f -db {f} -wtf')
        elif model == 'max_low':
            for f in ['max_high', 'max_high_above']:
                print(f'Model test on {f}')
                os.system(f'python network_api.py -tm {model} -ts 100 -f -db {f} -wtf')


if (sys.argv[-1] != 'r' and sys.argv[-1] != 'd') and not os.path.exists(db):
    runs = 1
    for i in range(runs):
        for db_name in dbs:
            if len(sys.argv) == 3 and db_name != sys.argv[2]:
                continue
            name = _random_name('prosit')
            print(f'\n\n\n{db_name} ({name}): {i+1}/{runs}')
            split = "-is" if len(_get_dbs(db_name, "filtered").distinct("accession")) == 1 else ""
            os.system(f'python network_api.py -t {name} -db {db_name} -bs 512 -e 100 -es 10 -lr 0.002 -s 2 -f -wtf {split}')
            for f in dbs:
                if f == db_name:
                    continue
                print(f'Model test on {f}')
                split = "-is" if len(_get_dbs(f, "filtered").distinct("accession")) == 1 else ""
                os.system(f'python network_api.py -tm {name} -f -db {f} -wtf {split} -s 2')

elif len(sys.argv) > 2 and sys.argv[-1] == 'r':
    print(f'Refining!')
    csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/prosit-*.csv') if len(f.split('-')) == 2]
    csv_files = [f for f in csv_files if os.path.exists(f.replace('.csv', '.h5'))]
    for csv_file in csv_files:
        csv = _import_table(csv_file)
        model = csv_file.split('/')[-1].split('.')[0]
        train_data = csv[csv['type'] == 'train']['db'][0]
        if not db in train_data:
            continue
        print(f'{model} ({train_data})')
        test_data = [f['db'] for i, f in csv[csv['type'] == 'test'].iterrows() if not f['db'] == train_data]
        for test_model in test_data:
            print(f'{model}-{test_model}')
            shutil.copy(f'{_get_path()}metadata/{model}.h5', f'{_get_path()}metadata/{model}-{test_model}.h5')
            params = _get_dbs('ms2ai', 'parameters').find_one({'_id': model})
            params['_id'] = f'{model}-{test_model}'
            _get_dbs('ms2ai', 'parameters').insert_one(params)
            split = "-is" if len(_get_dbs(test_model, "filtered").distinct("accession")) == 1 else ""
            os.system(f'python network_api.py -t {model}-{test_model} -db {test_model} -bs 512 -e 100 -es 10 -lr 0.002 -s 2 -f -wtf {split}')

elif len(sys.argv) > 2 and sys.argv[-1] == 'd':
    csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/prosit-*.csv') if len(f.split('-')) == 3]
    csv_files = [f for f in csv_files if os.path.exists(f.replace('.csv', '.h5'))]
    for csv_file in csv_files:
        csv = _import_table(csv_file)
        if db in csv[csv['type'] == 'train']['db'][0]:
            [os.remove(f) for f in [g.replace('\\', '/') for g in glob(f'{_get_path()}metadata/{"-".join(csv_file.split("/")[-1].split("-")[:2])}*')]]

elif os.path.exists(db):
    path = db.replace('\\', '/')
    csv_files = [f.replace('\\', '/') for f in glob(f'{path}{"" if path.endswith("/") else "/"}prosit-*.csv')]
    csv_files = sorted(csv_files, key=lambda x: len(x))
    model_names = [f.split('/')[-1].split('.')[0] for f in csv_files if len(f.split('/')[-1].split('.')[0]) == 17]
    csv_dict = {}
    with pd.ExcelWriter(f'G:/My Drive/Universitet/PhD/Papers/MS Data Review/output.xlsx') as writer:
        for model in model_names:
            models = [f for f in csv_files if model in f]
            df = pd.concat([_import_table(f) for f in models])
            df.to_excel(writer, sheet_name=df.iloc[0]['db'])
