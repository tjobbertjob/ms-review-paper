import os
from collections import defaultdict
from itertools import chain

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from fisher_py import RawFile


def _np_save(obj, path):
    with open(path, 'wb') as f:
        np.save(f, obj, allow_pickle=True)


def _np_load(path):
    with open(path, 'rb') as f:
        return np.load(f, allow_pickle=True)


def __create_file(path):
    if not os.path.exists(path):
        os.makedirs(path)


path = f'/data/tobias/PXD010595/'
filepath = f'/data/ms_review/fragementations/'
__create_file(filepath)

print(f'Calculating fragmentations')
frags = ['CID', 'HCD', 'ETD', 'ETciD', 'EThcD']
part_list = ['ETD', '3xHCD', 'DDA', '2xIT_2xHCD']
file_list = ['01974c_BH1-TUM_missing_first_8_01_01-Ø-1h-R4', '02079a_BE2-TUM_isoform_50_01_01-Ø-1h-R3',
             '02079a_BF4-TUM_isoform_64_01_01-Ø-1h-R3']

# _Frag: MS1, Frag_: MS2 energy average
mz_dict = {file_list[0]: {'CID': defaultdict(list), 'HCD': defaultdict(list), 'ETD': defaultdict(list),
                          'ETciD': defaultdict(list), 'EThcD': defaultdict(list),
                          '_ETD': [], '_DDA': [], '_2xIT_2xHCD': [], '_3xHCD': []},
           file_list[1]: {'CID': defaultdict(list), 'HCD': defaultdict(list), 'ETD': defaultdict(list),
                          'ETciD': defaultdict(list), 'EThcD': defaultdict(list),
                          '_ETD': [], '_DDA': [], '_2xIT_2xHCD': [], '_3xHCD': []},
           file_list[2]: {'CID': defaultdict(list), 'HCD': defaultdict(list), 'ETD': defaultdict(list),
                          'ETciD': defaultdict(list), 'EThcD': defaultdict(list),
                          '_ETD': [], '_DDA': [], '_2xIT_2xHCD': [], '_3xHCD': []}}
int_dict = {file_list[0]: {'CID': defaultdict(list), 'HCD': defaultdict(list), 'ETD': defaultdict(list),
                           'ETciD': defaultdict(list), 'EThcD': defaultdict(list),
                           '_ETD': [], '_DDA': [], '_2xIT_2xHCD': [], '_3xHCD': []},
            file_list[1]: {'CID': defaultdict(list), 'HCD': defaultdict(list), 'ETD': defaultdict(list),
                           'ETciD': defaultdict(list), 'EThcD': defaultdict(list),
                           '_ETD': [], '_DDA': [], '_2xIT_2xHCD': [], '_3xHCD': []},
            file_list[2]: {'CID': defaultdict(list), 'HCD': defaultdict(list), 'ETD': defaultdict(list),
                           'ETciD': defaultdict(list), 'EThcD': defaultdict(list),
                           '_ETD': [], '_DDA': [], '_2xIT_2xHCD': [], '_3xHCD': []}}


print(f'Getting MS2 Spectra')
def remove_precursor(scan, pre_mz, isolation_width, missing):
    mz = scan[0]
    ints = scan[1]
    pre_index = [i for i, f in enumerate(abs(mz - pre_mz) < isolation_width/2) if f]
    if not pre_index:
        # print('no precursor scan!')
        missing += 1
    pre_index = pre_index if pre_index else [len(mz)]
    mz = mz[np.r_[0:min(pre_index), max(pre_index)+1:len(mz)]]
    int = ints[np.r_[0:min(pre_index), max(pre_index):len(ints)]]

    return list(mz), list(int), missing


def get_filters(scan):
    frag_info = scan.split('@')[1].split('[')[0][:-1]
    if len(scan.split('@')) > 2:
        combo = scan.split('@')[2].split('[')[0][:-1][:-5]
        if combo == 'cid':
            method = 'ETciD'
        else:
            method = 'EThcD'
        energy = '-'.join([f.split('[')[0].strip()[-5:] for f in scan.split('@')[1:]])
    else:
        method = frag_info[:3].upper()
        energy = frag_info[3:]

    pre_mz = float(scan.split('@')[0].split(' ')[-1])
    return method, energy, pre_mz

print(f'Creating data!')
########## DATA CREATION ##########
if not os.path.exists(f'{filepath}data.txt'):
    for i, file in enumerate(file_list):
        for ii, part in enumerate(part_list):
            data = RawFile(f'{path}{file.replace("Ø", part)}/file.raw')
            # data = RawFile(r'E:\data\PXD010595\01974c_BH1-TUM_missing_first_8_01_01-DDA-1h-R4\file.raw')
            scan_numbers = data._ms2_scan_numbers.tolist()
            try:
                scans = [data.get_scan_from_scan_number(int(f)) for f in scan_numbers]
            except:
                continue
            no_precursor = 0
            for iii, scan in enumerate(scans):
                if iii % 1000 == 0:
                    print(f'File: {i + 1}/{len(file_list)}, Part: {ii + 1}/{len(part_list)}, Scan: {iii}/{len(scan_numbers)}, Missing precursor: {no_precursor}        ', end='\r')
                isolation_width = data._raw_file_access.get_scan_event_for_scan_number(scan_numbers[0]).get_reaction(0).isolation_width
                method, energy, pre_mz = get_filters(scan[3])
                mz, ints, no_precursor = remove_precursor(scan, pre_mz, isolation_width, no_precursor)
                mz_dict[file][method][energy].append(mz)
                int_dict[file][method][energy].append(ints)

    ## DONE WITH RAW FILE ##
    for file in file_list:
        for frag in ['CID', 'HCD', 'ETD', 'ETciD', 'EThcD']:
            for mz_energy in mz_dict[file][frag]:
                mz_dict[file][frag][mz_energy] = list(chain(*mz_dict[file][frag][mz_energy]))
            mz_dict[file][f'*{frag}'] = list(
                chain(*[mz_dict[file][frag][mz_energy] for mz_energy in mz_dict[file][frag]]))
            for int_energy in int_dict[file][frag]:
                int_dict[file][frag][int_energy] = list(chain(*int_dict[file][frag][int_energy]))
            int_dict[file][f'*{frag}'] = list(
                chain(*[int_dict[file][frag][int_energy] for int_energy in int_dict[file][frag]]))
        for frag in ['_ETD', '_DDA', '_2xIT_2xHCD', '_3xHCD']:
            mz_dict[file][frag] = list(chain(*mz_dict[file][frag]))
            int_dict[file][frag] = list(chain(*int_dict[file][frag]))
    _np_save({'int': int_dict, 'mz': mz_dict}, f'{filepath}data.txt')
print('Reading saved data!')
save_data = _np_load(f'{filepath}data.txt').item()
# Different energy versions are averaged to one specific value


########## FILE PLOT CREATION ##########
print('File fragmentation average comparison')
for f in file_list:
    plt.cla()
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 3.5))
    fig.subplots_adjust(wspace=0.2)
    for g, ax in zip(['mz', 'int'], [ax1, ax2]):
        blah = g.replace('mz', 'm/z').replace('int', 'intensity')
        blah_label = g.replace('mz', 'm/z').replace('int', 'log(intensity)')
        for frag in [f'*{fr}' for fr in frags]:
            # if not save_data[g][f]:
            #     continue
            if g == 'int':
                data = list(np.log(save_data[g][f][frag]))
            else:
                data = save_data[g][f][frag]
            sns.kdeplot(data, linewidth=1, ax=ax)
        ax.set_title(blah, pad=10, fontsize=11)
        ax.tick_params(axis='x', labelsize=10)
        ax.tick_params(axis='y', labelsize=10)
        ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        ax.set_xlabel(blah_label, fontsize=10)
        ax.set_ylabel('Density', fontsize=10)
        ax.legend(frags, prop=dict(size=10), loc="upper right")
    plt.savefig(f'{filepath}/{f}.png', bbox_inches='tight')
    print(f, 'saved')

# ######## FILE ENERGY PLOT CREATION ##########
print('File fragmentation energy comparison')
for f in file_list:
    plt.cla()
    fig, ((mz_1, int_1), (mz_2, int_2), (mz_3, int_3), (mz_4, int_4), (mz_5, int_5)) = plt.subplots(5, 2, figsize=(8, 11))
    fig.subplots_adjust(hspace=0.65, wspace=0.15)
    fig.suptitle(f'Comparison of m/z and intensities in MS2 for a single sample', fontsize=11, y=0.92)
    for mz_ax, int_ax, frag in zip([mz_1, mz_2, mz_3, mz_4, mz_5], [int_1, int_2, int_3, int_4, int_5], frags):
        for g in ['mz', 'int']:
            sorted_energy = sorted(save_data[g][f][frag].keys(), key=lambda x: float(x.split("-")[0]))
            for energy in sorted_energy:
                # if not save_data[g][f]:
                #     continue
                if g == 'int':
                    data = list(np.log(save_data[g][f][frag][energy]))
                else:
                    data = save_data[g][f][frag][energy]
                sns.kdeplot(data, linewidth=1, ax=eval(f'{g}_ax'))
            # Setup layout
            eval(f'{g}_ax').set_title(f'{g.replace("mz", "m/z").replace("int", "Intensity")} | {frag}', pad=12, fontsize=10)
            eval(f'{g}_ax').tick_params(axis='x', labelsize=9)
            eval(f'{g}_ax').tick_params(axis='y', labelsize=9)
            eval(f'{g}_ax').ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
            eval(f'{g}_ax').set_xlabel(g.replace("mz", "m/z").replace("int", "log(intensity)"), fontsize=9)
            eval(f'{g}_ax').set_ylabel('Density', fontsize=9)
            eval(f'{g}_ax').legend(sorted_energy, prop=dict(size=8), loc="upper right")
    plt.savefig(f'{filepath}/{f}_energy.png', bbox_inches='tight')
    print(f, 'saved')


########## FRAGMENTATION COMPARISON PLOT CREATION ##########
plt.cla()
fig, ((mz_1, int_1), (mz_2, int_2), (mz_3, int_3), (mz_4, int_4), (mz_5, int_5)) = plt.subplots(5, 2, figsize=(8, 11))
fig.subplots_adjust(hspace=0.65, wspace=0.15)
fig.suptitle(f'Comparison of m/z & intensity of peaks in MS2 for fragmentation methods across files', fontsize=11, y=0.92)
for mz_ax, int_ax, frag in zip([mz_1, mz_2, mz_3, mz_4, mz_5], [int_1, int_2, int_3, int_4, int_5], frags):
    for g in ['mz', 'int']:
        for file in file_list:
            if g == 'int':
                data = list(np.log(save_data[g][file][f'*{frag}']))
            else:
                data = save_data[g][file][f'*{frag}']
            sns.kdeplot(data, linewidth=1, ax=eval(f'{g}_ax'))
        # Setup layout
        eval(f'{g}_ax').set_title(f'{g.replace("mz", "m/z").replace("int", "Intensity")} | {frag}', pad=12, fontsize=10)
        eval(f'{g}_ax').tick_params(axis='x', labelsize=9)
        eval(f'{g}_ax').tick_params(axis='y', labelsize=9)
        eval(f'{g}_ax').ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        eval(f'{g}_ax').set_xlabel(g.replace("mz", "m/z").replace("int", "log(intensity)"), fontsize=9)
        eval(f'{g}_ax').set_ylabel('Density', fontsize=9)
        eval(f'{g}_ax').legend([f.split('-')[0] for f in file_list], prop=dict(size=8), loc="upper right")

plt.savefig(f'{filepath}/fragmentation_comparison.png', bbox_inches='tight')
print('Comparison saved')
