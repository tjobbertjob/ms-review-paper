import argparse
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from pymongo import MongoClient


files = MongoClient()['ms2ai']['files']
dbs = ['gen_pt19', 'gen_pt17', 'gen_limit', 'gen_wide', 'min_high', 'min_below', 'max_low', 'max_above', 'org_human', 'org_mouse', 'ins_qexactive', 'ins_orbitrap', 'grad_low', 'grad_high']
dbs = ['gen_wide']

queries = {'gen_pt19': '"accession = PXD010595" "modified sequence exists 1" "score > 100"',
           'gen_pt17': '"accession = PXD004732" "modified sequence exists 1" "score > 100"',
           'gen_limit': '"accession nin PXD004732 PXD010595" "modified sequence exists 1" "score > 100"',
           'gen_wide': '"accession nin PXD004732 PXD010595" "modified sequence exists 1" "score > 150"',
           'ins_qexactive': '"accession nin PXD004732 PXD010595" "instruments regex Exactive" "modified sequence exists 1" "score > 150"',
           'ins_orbitrap': '"accession nin PXD004732 PXD010595" "instruments regex Orbitrap" "modified sequence exists 1" "score > 150"',
           'org_human': '"accession nin PXD004732 PXD010595" "organisms = Homo sapiens (human)" "modified sequence exists 1" "score > 150"',
           'org_mouse': '"accession nin PXD004732 PXD010595" "organisms = Mus musculus (mouse)" "modified sequence exists 1" "score > 150"',
           'upper': '"accession nin PXD004732 PXD010595" "m/z > 1300" "modified sequence exists 1" "score > 100"',
           'lower': '"accession nin PXD004732 PXD010595" "m/z < 360" "modified sequence exists 1" "score > 100"',
           'filter': "accession nin PXD004732 PXD010595" "modified sequence exists 1" "score > 100"'',
           'grad_high': '"accession nin PXD004732 PXD010595" "gradient length >= 100" "modified sequence exists 1" "score > 150"',
           'grad_low': '"accession nin PXD004732 PXD010595" x½"gradient length <= 60" "modified sequence exists 1" "score > 150"'}
or_queries = {'filter': '"m/z < 360" "m/z > 1300"'}

mongo_queries = {'gen_pt19': {'accession': 'PXD010595', 'score': {'$gte': 100}, 'modified sequence': {'$exists': True}},
                 'gen_pt17': {'accession': 'PXD004732', 'score': {'$gte': 100}, 'modified sequence': {'$exists': True}},
                 'gen_limit': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'score': {'$gte': 100}, 'modified sequence': {'$exists': True}},
                 'gen_wide': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'ins_qexactive': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'instruments': {'$regex': 'Exactive'}}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'ins_orbitrap': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'instruments': {'$regex': 'Orbitrap'}}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'org_human': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'organisms': 'Homo sapiens (human)'}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'org_mouse': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'organisms': 'Mus musculus (mouse)'}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'max_low': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'ms1 filter max': {'$lte': 1300}}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'max_above': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'm/z': {'$gt': 1300}, 'score': {'$gte': 100}, 'modified sequence': {'$exists': True}},
                 'min_high': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'ms1 filter min': {'$gte': 360}}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'min_below': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'm/z': {'$lt': 360}, 'score': {'$gte': 100}, 'modified sequence': {'$exists': True}},
                 'grad_high': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'gradient length': {'$gte': 100}}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}},
                 'grad_low': {'accession': {'$nin': ['PXD004732', 'PXD010595']}, 'identifier': {'$in': files.find({'gradient length': {'$lte': 60}}).distinct('identifier')}, 'score': {'$gte': 150}, 'modified sequence': {'$exists': True}}
                 }

limits = {'gen_pt19': 750000,
          'gen_pt17': 750000,
          'gen_limit': 750000,
          'gen_wide': 2000000,
          'ins_qexactive': 2000000,
          'ins_orbitrap': 2000000,
          'org_human': 2000000,
          'org_mouse': 2000000,
          'max_low': 1500000,
          'max_above': 125000,
          'min_high': 1500000,
          'min_below': 125000,
          'grad_high': 2000000,
          'grad_low': 2000000}

# gen_pt19 380613
# gen_pt17 664975
# gen_limit 18818022
# gen_wide 5858542
# min_high 3047057
# min_below 387285
# max_low 1748589
# max_above 135396
# org_human 9370454
# org_mouse 2978287
# ins_qexactive 8130847


def filter_and_copy():
    import os
    for f in dbs:
        print(f)
        try:
            qs = '' if f not in queries else f'-q {queries[f]}'
            oqs = '' if f not in or_queries else f'-oq {or_queries[f]}'
            os.system(f'python filter_api.py -c "linear rt" {qs} {oqs} -db ms2ai -lo {limits[f]} -f')
            os.system(f'python utility_api.py -c ms2ai {f} --filtered -kr --physical -f')
        except Exception as E:
            print(f'python filter_api.py -c "linear rt" {qs} {oqs} -db ms2ai -lo {limits[f]} -f', E)
            print(f'python utility_api.py -c ms2ai {f} --filtered -kr -f', E)


def score_plot():
    score = [f['score'] for f in MongoClient()['ms2ai']['peptides'].find({'score': {'$exists': True}})]
    fig, ax = plt.subplots()
    plt.hist(x=score, cumulative=-1, alpha=1, histtype='stepfilled', bins=np.arange(round(min(score), -1), 250, 0.25), color='royalblue')
    plt.xlabel('Scores');plt.ylabel('Peptides (millions)')
    ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x / 1e6)))
    plt.grid(visible=True);plt.tight_layout();plt.savefig('figure 2.png', dpi=500)


def compute_limits():
    dbs = ['gen_pt19', 'gen_pt17', 'gen_limit', 'gen_wide', 'min_high', 'min_below', 'max_low', 'max_above', 'org_human', 'org_mouse', 'ins_qexactive', 'ins_orbitrap', 'grad_low', 'grad_high']
    for f in dbs:
        print(f, MongoClient()['ms2ai']['peptides'].count_documents(mongo_queries[f]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Cloud Computing Parser')
    parser.add_argument('-f', '--filter_and_copy', help="a", action='store_true')
    parser.add_argument('-s', '--score_plots', help="a", action='store_true')
    parser.add_argument('-c', '--compute_limits', help="a", action='store_true')

    input_args = parser.parse_args()
    if input_args.filter_and_copy:
        filter_and_copy()
    if input_args.score_plots:
        score_plot()
    if input_args.compute_limits:
        compute_limits()
