from pymongo import MongoClient
import pandas as pd
import numpy as np
import collections
import matplotlib.pyplot as plt
from itertools import chain


def len_plot_deprecated():
    for f in dbs:
        try:
            a = ([{'$group': {'_id': 0,
                              'average_lrt': {'$avg': '$label'},
                              'average_mz': {'$avg': '$m/z'},
                              'average_rt': {'$avg': '$retention time'},
                              'average_length': {'$avg': '$length'}}}])
            b = MongoClient()[f]['filtered']
            print(f, 'average_lrt', list(b.aggregate(a))[0]['average_lrt'],
                  'average_mz', list(b.aggregate(a))[0]['average_mz'],
                  'average_rt', list(b.aggregate(a))[0]['average_rt'],
                  'average_length', list(b.aggregate(a))[0]['average_length'], )
        except:
            print(f)

    lens = collections.defaultdict(list)
    [lens[f['length']].append(f['linear rt']) for f in MongoClient()['ms2ai']['peptides'].find({}, {'_id': 0, 'linear rt': 1, 'length': 1})[1:]]
    stds = {f: np.std(lens[f]) for f in lens}
    stds = collections.OrderedDict(sorted(stds.items()))
    stds = {f: stds[f] for f in stds if 5 < f < 30}

    means = {f: np.mean(lens[f]) for f in lens}
    means = collections.OrderedDict(sorted(means.items()))
    means = {f: means[f] for f in means if 5 < f < 30}
    plt.cla()
    plt.plot(stds.keys(), stds.values())
    plt.plot(means.keys(), means.values())
    plt.savefig('len.png')


def fix_name(name):
    name = name.replace('gen_wide', 'Wide').replace('gen_limit', 'Limit').replace('gen_pt17', 'PT17')\
        .replace('gen_pt19', 'PT19').replace('org_mouse', 'Mouse').replace('org_human', 'Human')\
        .replace('ins_orbitrap', 'Orbitrap').replace('ins_qexactive', 'Q. Exactive').replace('grad_high', 'Long')\
        .replace('grad_low', 'Short').replace('random', 'Random').replace('min_high', '>360').replace('max_low', '<1300')
    return name


def plots(db, dbs, metric, metric_name, colors, path, xl):
    i_df = {}
    # Initial Plots
    for f in db:
        s = [g for g in dbs if g.split('_')[0] == f]
        a = {ff: pd.read_excel(xl, ff)[:len(s)+2] for ff in s}
        results = {}
        results['Train'] = [a[ff][a[ff]['type'] == 'train'][metric].iloc[0] for ff in a]
        results['Validation'] = [a[ff][a[ff]['type'] == 'validation'][metric].iloc[0] for ff in a]
        [results.update({f'{fix_name(g)}\n(test)': [a[ff][(a[ff]['type'] == 'test') & (a[ff]['db'] == g)][metric].iloc[0] for ff in a]}) for g in s]
        df = pd.DataFrame(results)
        df2 = df.transpose()
        df2.columns = [fix_name(g) for g in s]
        i_df[f] = df2
        df2.plot(kind='bar', stacked=False, rot=0, color=colors, **{'width': 0.8})
        plt.ylabel(metric_name, fontsize=13)
        plt.legend(fontsize=13, loc='upper right', ncol=int(max(len(s)/2, 1)))
        plt.tight_layout()
        plt.xticks(fontsize=12)
        plt.yticks(fontsize=11)
        # plt.show()
        plt.savefig(f'{path}review/{f}_{metric_name}.png')

    # Filter plots
    s = ['gen_pt17', 'gen_pt19']
    a = {ff: pd.read_excel(xl, ff) for ff in s}
    results = {}
    results['Initial'] = [a[ff][a[ff]['type'] == 'test'][metric].iloc[0] for ff in a]
    results['Lower'] = [a[ff][(a[ff]['type'] == 'test') & (a[ff]['db'] == 'lower')][metric].iloc[0] for ff in a]
    results['Upper'] = [a[ff][(a[ff]['type'] == 'test') & (a[ff]['db'] == 'upper')][metric].iloc[0] for ff in a]
    df = pd.DataFrame(results)
    df2 = df.transpose()
    df2.columns = [fix_name(f) for f in s]
    i_df['filter'] = df2
    df2.plot(kind='bar', stacked=False, rot=0, color=colors, **{'width': 0.8})
    plt.ylabel(metric_name, fontsize=14)
    plt.legend(fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=11)

    # plt.ylim([0, 0.15])
    plt.tight_layout()
    # plt.show()
    plt.savefig(f'{path}review/filter_{metric_name}.png')


    # Refinement plots
    def overlapped_bar(df, max_val, ax, xlab):
        """Like a stacked bar chart except bars on top of each other with transparency"""
        N = len(df)
        M = len(df.columns)
        indices = np.arange(N)
        colors = ['blue', 'orange']
        width = [0.95, 0.8]
        alpha = [1, 0.75]
        for i, label, color in zip(range(M), df.columns, colors):
            kwargs = {'color': color, 'label': label}
            ax.bar(indices, df[label], width=width[i], alpha=alpha[i] if i else 1, **kwargs)
            ax.set_xticks(indices, ['{}'.format(fix_name(idx)) for idx in df.index.values], fontsize=12)
        ax.set_ylabel(metric_name, fontsize=14)
        ax.set_xlabel('Model', fontsize=14)
        ax.tick_params(axis="y", labelsize=11)
        ax.set_title(f'Performance on the {xlab} dataset')
        # ax.set_ylim(0, max_val)
        ax.legend(fontsize=14 if len(df) == 2 else 12)


    db = ['gen', 'org', 'grad', 'ins']
    size = {'gen': [2, 2], 'org': [2, 1], 'grad': [2, 1], 'ins': [2, 1], 'random': [1, 1]}
    r_df = {}
    for f in db:
        s = [g for g in dbs if g.split('_')[0] == f]
        dfs = {}
        fig, ax = plt.subplots(ncols=size[f][0], nrows=size[f][1], figsize=(10, 10 if f == 'gen' else 8))
        l_ax = list(chain(*ax)) if f == 'gen' else ax
        for i, g in enumerate(s):
            initial = [pd.read_excel(xl, gg)[(pd.read_excel(xl, gg)['type'] == 'test') & (pd.read_excel(xl, gg)['db'] == g)][metric].iloc[0] for gg in s]
            refine = [pd.read_excel(xl, gg)[(pd.read_excel(xl, gg)['type'] == 'test') & (pd.read_excel(xl, gg)['db'] == g)][metric].iloc[1] for gg in s if gg != g]
            refine = refine[:i] + [0] + refine[i:]
            dfs[g] = pd.DataFrame(np.matrix([initial, refine]).T, columns=['Initial testing', 'Refinement testing'], index=pd.Index([fix_name(ff) for ff in s], name='Index'))
        max_val = max([max([max(g[gg]) for gg in g.columns]) for g in dfs.values()])
        max_val = (np.ceil(max_val * 100) + 1) / 100
        r_df[f] = dfs
        for i, g in enumerate(dfs):
            df = dfs[g]
            overlapped_bar(df, max_val, l_ax[i], fix_name(g))
        plt.tight_layout()
        plt.savefig(f'{path}review/{f}-refinement_{metric_name}.png')

def epochs():
    # Epochs
    db = ['gen', 'org', 'grad', 'ins']
    e_df = {}
    for f in db:
        s = [g for g in dbs if g.split('_')[0] == f]
        a = {ff: pd.read_excel(f'{path}output.xlsx', ff) for ff in s}
        ggg = [[a[gg][(a[gg]['db'] == g) & (a[gg]['epochs'] != 0)]['epochs'].iloc[0] for gg in a] for g in s]
        df = pd.DataFrame(ggg)
        df.index = [fix_name(gg) for gg in s]
        df.columns = [fix_name(gg) for gg in s]
        e_df[f] = df

    e_dfs = {}
    for f in e_df:
        s = [g for g in dbs if g.split('_')[0] == f]
        a = [list((e_df[f].iloc[i] / e_df[f].iloc[i][i]).round(3)) for i in range(len(e_df[f]))]
        df = pd.DataFrame(a)
        col_means = df.mean(axis=1)
        row_means = df.mean(axis=0)
        df = df.append(pd.Series(row_means, name='Means'))
        mean_val = np.mean(row_means)
        df['Means'] = list(col_means) + [mean_val]
        df.index = [fix_name(gg) for gg in s] + ['Means']
        df.columns = [fix_name(gg) for gg in s] + ['Means']
        e_dfs[f] = df

    with pd.ExcelWriter(f'{path}epochs.xlsx') as writer:
        for df in e_dfs:
            e_dfs[df].to_excel(writer, sheet_name=df)


if __name__ == '__main__':
    dbs = ['random', 'gen_pt17', 'gen_pt19', 'gen_limit', 'gen_wide', 'min_high', 'max_low', 'org_human', 'org_mouse',
           'grad_high', 'grad_low', 'ins_orbitrap', 'ins_qexactive']
    db = ['gen', 'org', 'grad', 'ins']
    colors = ['blue', 'orange', 'green', 'firebrick']
    path = 'E:/data/metadata/'
    xl = pd.ExcelFile(f'{path}output.xlsx')
    plots(db, dbs, 'timedelta', 'ΔRT', colors, path, xl)
    plots(db, dbs, 'mean_absolute_error', 'MAE', colors, path, xl)
    plots(db, dbs, 'loss', 'MSE', colors, path, xl)
