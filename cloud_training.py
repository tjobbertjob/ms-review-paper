import argparse
import os
import random
import shutil
import string
import sys
from glob import glob
from itertools import chain
from json import loads, dump

import pandas as pd
from pymongo import MongoClient

from extractor.common import _import_table
from storage.db_init import _get_dbs
from storage.utils import _get_path


def _random_name(model):
    characters = string.ascii_letters + string.digits
    return f"{model}-{''.join(random.choice(characters) for i in range(10))}"


def _save_params(path, params):
    params_file = loads(open(f'{path}metadata/parameters.txt', 'r').read()) if os.path.exists(f'{path}metadata/parameters.txt') else {}
    with open(f'{path}metadata/parameters.txt', 'w') as file:
        dump({**params_file, **{params['_id']: params}}, file, indent=4)


def _test_oob():
    csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/ms2ai_prosit-*.csv') if len(f.split('-')) == 2]
    for csv_file in csv_files:
        csv = _import_table(csv_file)
        model = csv[csv['type'] == 'train']['db'][0]
        if model in ['gen_pt17', 'gen_pt19']:
            os.system(f'python network_api.py -tm {csv_file.split("/")[-1].split(".")[0]} -ts 100 -f -db upper -wtf -ph')
            os.system(f'python network_api.py -tm {csv_file.split("/")[-1].split(".")[0]} -ts 100 -f -db lower -wtf -ph')
        # if model == 'min_high':
        #     print(f'Model test on min_high')
        #     os.system(f'python network_api.py -tm {csv_file.split("/")[-1].split(".")[0]} -ts 100 -f -db min_below -wtf -ph')
        # elif model == 'max_low':
        #     print(f'Model test on max_above')
        #     os.system(f'python network_api.py -tm {csv_file.split("/")[-1].split(".")[0]} -ts 100 -f -db min_below -wtf -ph')


def _training(dbs, input_args):
    if input_args.train_check:
        csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/ms2ai_prosit-*.csv') if len(f.split('-')) == 2]
        csv_files = [f for f in csv_files if os.path.exists(f.replace('.csv', '.h5'))]
        pre_trained = _get_dbs('ms2ai', 'parameters').find({'_id': {'$in': [f.split('/')[-1].split('.')[0] for f in csv_files]}}).distinct('database')
        # pre_trained = [f[f['type'] == 'train']['db'].iloc[0] for f in [_import_table(f) for f in csv_files]]
    for db_name in [f for f in dbs if _get_dbs(f, 'peptides').find_one() and (f not in pre_trained if input_args.train_check else True)]:
        preface = db_name.split('_')[0]
        name = _random_name('ms2ai_prosit')
        print(f'\n\n{db_name}')
        split = " -is" if len(_get_dbs(db_name, "filtered").distinct("accession")) == 1 else ""
        os.system(f'python network_api.py -t {name} -db {db_name} -bs 512 -e 100 -es 20 -lr {input_args.learning_rate} -s {seed_dict[db_name]} -f -wtf -sos -ph{split}')
        for f in [f for f in dbs if f.split('_')[0] == preface and f != db_name]:
            print(f'Model test on {f}')
            split = "-is" if len(_get_dbs(f, "filtered").distinct("accession")) == 1 else ""
            os.system(f'python network_api.py -tm {name} -f -db {f} -wtf {split} -s {seed_dict[db_name]} -ph')


def _testing():
    print(f'testing!')
    csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/ms2ai_prosit-*.csv') if len(f.split('-')) == 2]
    csv_files = [f for f in csv_files if os.path.exists(f.replace('.csv', '.h5'))]
    for csv_file in csv_files:
        csv = _import_table(csv_file)
        train_data = csv[csv['type'] == 'train']['db'][0]
        prev_tests = list(csv[csv['type'] == 'test']['db'])
        for f in [f for f in dbs if f.split('_')[0] == train_data.split('_')[0] and f != train_data and f not in prev_tests]:
            print(f'Model test on {f}')
            split = "-is" if len(_get_dbs(f, "filtered").distinct("accession")) == 1 else ""
            name = csv_file.split('/')[-1].split('.')[0]
            os.system(f'python network_api.py -tm {name} -f -db {f} -wtf {split} -s {seed_dict[f]} -ph')


def _refining(dbs, input_args):
    print(f'Refining!')
    csv_files = [f.replace('\\', '/') for f in glob(f'{_get_path()}metadata/ms2ai_prosit-*.csv') if len(f.split('-')) == 2]
    csv_files = [f for f in csv_files if os.path.exists(f.replace('.csv', '.h5'))]
    for csv_file in csv_files:
        csv = _import_table(csv_file)
        model = csv_file.split('/')[-1].split('.')[0]
        train_data = csv[csv['type'] == 'train']['db'][0]
        if train_data not in dbs or (input_args.refine_from_to and train_data != input_args.refine_from_to[0]):
            continue
        print(f'{model} ({train_data})')
        test_data = [f['db'] for i, f in csv[csv['type'] == 'test'].iterrows() if not f['db'] == train_data]
        test_data = [f for f in test_data if f == input_args.refine_from_to[1]] if input_args.refine_from_to else test_data
        for test_model in test_data:
            model_name = f'{model}-{test_model}' if not input_args.seed else f'{model}-{test_model}-{input_args.seed}'
            if (os.path.exists(f'{input_args.path}metadata/{model_name}.csv') and input_args.refine_non_refined) or ('below' in test_model or 'above' in test_model):
                continue
            print(f'{model}-{test_model}')
            if not os.path.exists(f'{_get_path()}metadata/{model_name}.h5'):
                shutil.copy(f'{_get_path()}metadata/{model}.h5', f'{_get_path()}metadata/{model_name}.h5')
            if not _get_dbs('ms2ai', 'parameters').find_one({'_id': f'{model_name}'}):
                params = _get_dbs('ms2ai', 'parameters').find_one({'_id': model})
                if not params:
                    print(f'{model} not found in parameters. Delete the model files and re-train initial model')
                    continue
                params['_id'] = f'{model_name}'
                _get_dbs('ms2ai', 'parameters').insert_one(params)
            split = " -is" if len(_get_dbs(test_model, "filtered").distinct("accession")) == 1 else ""
            os.system(f'python network_api.py -t {model_name} -db {test_model} -bs 512 -e 100 -es 20 -lr {input_args.learning_rate} -s {seed_dict[test_model]} -f -tv -wtf -sos{split}')


def _check(path, dbs):
    csv_files = [f.replace('\\', '/') for f in glob(f'{path}metadata/{"" if path.endswith("/") else "/"}ms2ai_prosit-*.csv')]
    models = [f.split('/')[-1].split('.')[0] for f in csv_files]

    missing_parameters = [f for f in models if f not in _get_dbs('ms2ai', 'parameters').find({'_id': {'$in': models}})]
    initial = [f for f in _get_dbs('ms2ai', 'parameters').find({'_id': {'$in': [f for f in models if len(f.split('-')) == 2]}}) if f['database'] in dbs]
    print(f"Initial models missing: {', '.join([item for item in dbs if item not in [f['database'] for f in initial]])}")

    missing_refines = [[f'{f["database"]}-{g}' for g in dbs if g != f['database'] and g.split('_')[0] == f['database'].split('_')[0] and not os.path.exists(f'{path}metadata/{f["_id"]}-{g}.h5')] for f in initial]
    print(f'Refined models missing: {", ".join(list(chain(*missing_refines)))}')


def _combine(path, input_args):
    csv_files = [f.replace('\\', '/') for f in glob(f'{path}metadata/{"" if path.endswith("/") else "/"}ms2ai_prosit-*.csv')]
    csv_files = sorted(csv_files, key=lambda x: len(x))
    model_names = [f.split('/')[-1].split('.')[0] for f in csv_files if len(f.split('/')[-1].split('.')[0]) == 23]
    with pd.ExcelWriter(f'{path}metadata/output.xlsx') as writer:
        for model in model_names:
            models = [f for f in csv_files if model in f]
            df = pd.concat([_import_table(f) for f in models])
            df.to_excel(writer, sheet_name=df.iloc[0]['db'])


def _id_testing():
    print('In order to do id_testing you need to run the following commands in the MS2AI directory '
          '\npython network_api.py -tm ms2ai_prosit-ICyuGQXHTg -id 25 -db gen_pt17 -s 2 -v -is'
          '\npython network_api.py -tm ms2ai_prosit-I7Y5y3xDqm -id 25 -db gen_pt19 -s 2 -v -is'
          '\npython network_api.py -tm ms2ai_prosit-5eJyFnNmrj -id 25 -db gen_limit -s 2 -v'
          '\npython network_api.py -tm ms2ai_prosit-vZVSbEIF6Z -id 25 -db gen_wide -s 2 -v'
          '\npython network_api.py -tm ms2ai_prosit-ICyuGQXHTg -id 25 -db upper -v -ts 100'
          '\npython network_api.py -tm ms2ai_prosit-ICyuGQXHTg -id 25 -db lower -v -ts 100'
          '\npython network_api.py -tm ms2ai_prosit-I7Y5y3xDqm -id 25 -db lower -v -ts 100'
          '\npython network_api.py -tm ms2ai_prosit-I7Y5y3xDqm -id 25 -db upper -v -ts 100'
          '\npython network_api.py -tm ms2ai_prosit-ICyuGQXHTg -id 25 -db filter -v -ts 100'
          '\npython network_api.py -tm ms2ai_prosit-I7Y5y3xDqm -id 25 -db filter -v -ts 100')


def _setup_args():
    parser = argparse.ArgumentParser(description='Cloud Computing Parser')
    parser.add_argument('-t', '--training', help="a", action='store_true')
    parser.add_argument('-x', '--train_check', help="a", action='store_true')
    parser.add_argument('-te', '--testing', help="a", action='store_true')
    parser.add_argument('-r', '--refining', help="", action='store_true')
    parser.add_argument('-rtf', '--refine_from_to', help="", type=str, nargs='+', default=[])
    parser.add_argument('-rnr', '--refine_non_refined', help="", action='store_true')
    parser.add_argument('-c', '--combine', help="", action='store_true')
    parser.add_argument('-ch', '--check', help="", action='store_true')
    parser.add_argument('-o', '--oob', help="", action='store_true')
    parser.add_argument('-p', '--path', help="", type=str)
    parser.add_argument('-s', '--seed', help="", type=int)
    parser.add_argument('-dbs', '--databases', help="", type=str, nargs='+', metavar='', default=[])
    parser.add_argument('-lr', '--learning_rate', type=float, help='', metavar="", default=0.001)
    return parser


def _compile_args(parser):
    try:
        input_args = parser.parse_args()
        input_args.path = _get_path()
        return input_args
    except:
        quit(parser.print_help())


if __name__ == '__main__':
    input_args = _compile_args(_setup_args())
    dbs = ['random', 'gen_pt19', 'gen_pt17', 'gen_limit', 'gen_wide', 'org_human', 'org_mouse', 'grad_high', 'grad_low', 'ins_orbitrap', 'ins_qexactive'] if not input_args.databases else input_args.databases
    seed_dict = {'random': 2, 'gen_pt19': 2, 'gen_pt17': 2, 'gen_limit': 2, 'gen_wide': 2, 'min_high': 2, 'max_above': 2, 'min_below': 2, 'max_low': 2, 'org_human': 2, 'org_mouse': 2, 'grad_high': 2, 'grad_low': 2, 'ins_orbitrap': 2, 'ins_qexactive': 2}
    if input_args.seed:
        seed_dict = {f: input_args.seed for f in seed_dict}

    if input_args.training:
        _training(dbs, input_args)

    if input_args.testing:
        _testing()

    if input_args.oob:
        _test_oob()

    if input_args.refining or input_args.refine_non_refined or input_args.refine_from_to:
        _refining(dbs, input_args)

    path = f'{_get_path()}metadata/' if not input_args.path else input_args.path
    if input_args.check:
        _check(path, dbs)

    if input_args.combine:
        _combine(path, input_args)

    _id_testing()
